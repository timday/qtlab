import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
  id: window
  visible: true
  width: 800
  height: 600

  onClosing: {console.log('QML closing');Qt.quit();}

  Rectangle {
    id: main
    x: 0      
    y: 0
    width: parent.width
    height: parent.height
    color: '#ff0000'
  }
}
