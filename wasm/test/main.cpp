#include <QApplication>
#include <QObject>
#include <QQmlApplicationEngine>

void log_QApplication_aboutToQuit() {
  qInfo() << "QApplication aboutToQuit";
}

void log_QmlApplicationEngine_quit() {
  qInfo() << "QQmlApplicationEngine quit";
}

int main(int argc,char* argv[]) {

  int rtn=0;
  
  {
    QApplication app(argc,argv);
    //app.setQuitOnLastWindowClosed(false); // This just makes it not quit
    QObject::connect(&app,&QApplication::aboutToQuit,log_QApplication_aboutToQuit);
    
    QQmlApplicationEngine* engine=new QQmlApplicationEngine("qrc:///main.qml",&app);
    QObject::connect(engine,&QQmlApplicationEngine::quit,log_QmlApplicationEngine_quit);
    
    rtn=app.exec();
    qInfo() << "Returned from app.exec()";
    
    delete engine;
    
    qInfo() << "Deleted QQmlApplicationEngine";
  }

  qInfo() << "QApplication out of scope";
  
  return rtn;
}
