cmake
-----
https://doc.qt.io/qt-6/cmake-get-started.html

Needed to bump cmake version; is in buster-backports

emsdk
-----
See https://doc-snapshots.qt.io/qt6-dev/wasm.html

emsdk lives in ../../emsdk

`git pull` to update, then
```
    ./emsdk install 2.0.14
    ./emsdk activate 2.0.14
```

`source /path/to/emsdk/emsdk_env.sh` to set environment

`(source emsdk_env.sh ; em++ --version)` confirms version.

3D
--
Initially confused... quick3d seems to be the new thing "replacing" Qt3D
https://www.qt.io/blog/2019/08/14/introducing-qt-quick-3d-high-level-3d-api-qt-quick
But test-example-simple-qml was Qt3D. Doh!

Want to revisit https://forum.qt.io/topic/117143/what-do-i-need-to-do-to-get-quick3d-wasm-working/2
Which uses ~/Qt/Examples/Qt-6.2.1/quick3d/hellocube

