import QtQuick 2.3

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15

/*
Rectangle {
  id: main
  width: 800      
  height: 600
  color: '#ff0000'
*/

  Entity {
    id: sceneRoot

    Camera {
      id: camera
      projectionType: CameraLens.PerspectiveProjection
      fieldOfView: 45
      aspectRatio: 16/9
      nearPlane : 0.1
      farPlane : 1000.0
      position: Qt.vector3d( 0.0, 0.0, -40.0 )
      upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
      viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
    }

    components: [
        RenderSettings {
            activeFrameGraph: ForwardRenderer {
                clearColor: Qt.rgba(0, 0.5, 1, 1)
                camera: camera
                showDebugOverlay: true
            }
        },
        // Event Source will be set by the Qt3DQuickWindow
        InputSettings { }
    ]

    PhongMaterial {
        id: material
    }

    TorusMesh {
        id: torusMesh
        radius: 5
        minorRadius: 1
        rings: 100
        slices: 20
    }

    Transform {
        id: torusTransform
        scale3D: Qt.vector3d(1.5, 1, 0.5)
        rotation: fromAxisAndAngle(Qt.vector3d(1, 0, 0), 45)
    }

    Entity {
        id: torusEntity
        components: [ torusMesh, material, torusTransform ]
    }


  }   
/*
  Text {
    anchors.centerIn: main
    text: 'Hello from QML'
    color: '#ffff00'
  }
}
*/
