Similar to test 1 but simpler; use vanilla QQuickView.

Supposed to be a minimal app.

But seems to crash on exit in shutdown in XQueryExtension in some libGL thing on __run_exit_handlers.

Not necessarily a Qt issue.

Builds `test.js` and `test.wasm` but running with
```
../../../emsdk/upstream/emscripten/emrun --browser=firefox test.js

```
doesn't seem to do anything.

qmake
-----
https://wiki.qt.io/Qt_6.2_Known_Issues mentions an edit needed to avoid `Project ERROR: Could not find feature sse2.` message.

3D
--
Looks like simple examples:
`~/Qt/Examples/Qt-6.2.1/qt3d/simple-qml/`
`~/Qt/Examples/Qt-6.2.1/qt3d/phong-cubes/`

Ah... this looks important: https://www.kdab.com/getting-your-3d-ready-for-qt-6/
