TEMPLATE = app

CONFIG += debug
CONFIG += qtquickcompiler

QT += core gui quick qml 3dcore 3drender 3dinput 3dquick 3dquickextras

TARGET = test

SOURCES += main.cpp

RESOURCES += resources.qrc

cache()
