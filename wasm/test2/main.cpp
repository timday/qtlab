#include <QGuiApplication>
#include <QQuickView>

int main(int argc,char* argv[]) {
  
  QGuiApplication app(argc,argv);

  QQuickView viewer(QUrl("qrc:///main.qml"));

  viewer.show();

  return app.exec();
}
