TEMPLATE = app

TARGET = testbench

CONFIG += debug
CONFIG += qtquickcompiler
CONFIG += c++17
CONFIG += qmltypes

QT += core gui quick network qml   # quick3d

QML_IMPORT_NAME = Testbench
QML_IMPORT_MAJOR_VERSION = 1

SOURCES += main.cpp # imageloader.cpp  # TODO: Missing?  Needs git add?

# HEADERS += imageloader.h

RESOURCES += resources.qrc

DESTDIR = build
OBJECTS_DIR = build/obj
MOC_DIR = build/moc
RCC_DIR = build/rcc

cache()
