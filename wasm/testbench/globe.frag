#version 440

layout(location = 0) in vec2 qt_TexCoord0;
layout(location = 0) out vec4 fragColor;

layout(binding = 1) uniform sampler2D source;

layout(std140, binding = 0) uniform buf {
  mat4 qt_Matrix;
  float qt_Opacity;
  vec2 delta;
  float scale;
  float rotation;
} ubuf;

const float c_pi = 3.14159265358979323846264;
const float c_halfPi = c_pi*0.5;
const float c_twoPi = c_pi*2.0;

const int MULTISAMPLING=2;

const vec3 light=normalize(vec3(-1.0,-1.0,3.0));

vec3 tophysical(vec3 perceptual)
{
  bvec3 cutoff=lessThan(perceptual,vec3(0.04045));
  vec3 higher=pow((perceptual+vec3(0.055))/vec3(1.055),vec3(2.4));
  vec3 lower=perceptual/vec3(12.92);
      
  return vec3(not(cutoff))*higher + vec3(cutoff)*lower;
}

vec4 tophysical(vec4 perceptual) {
  if (perceptual.a==0.0) {
    return vec4(0.0,0.0,0.0,0.0);
  } else {
    return vec4(tophysical(perceptual.rgb/perceptual.a)*perceptual.a,perceptual.a);
  }
}

vec3 toperceptual(vec3 physical) {
  bvec3 cutoff=lessThan(physical,vec3(0.0031308));
  vec3 higher=vec3(1.055)*pow(physical,vec3(1.0/2.4))-vec3(0.055);
  vec3 lower=physical*vec3(12.92);
  return vec3(not(cutoff))*higher + vec3(cutoff)*lower;
}
  
vec4 toperceptual(vec4 physical) {
  if (physical.a==0.0) {
    return vec4(0.0,0.0,0.0,0.0);
  } else {
    return vec4(toperceptual(physical.rgb/physical.a)*physical.a,physical.a);
  }
}

// WebGL should default to premultiplied alpha
vec4 over(vec4 front,vec4 back) {
  return front+back*(1.0-front.a);
}

vec4 trace(float x,float y) {

  float x2=x*x;
  float y2=y*y;
  float r2=x2+y2;

  vec4 result=vec4(0.0,0.0,0.0,0.0);

  if (r2<1.0) {

    float z=sqrt(1.0-r2);
    float longitude=mod(atan(x,z)-ubuf.rotation+c_pi,c_twoPi)-c_pi;
    float latitude=asin(y);

    float px=0.5+longitude/c_twoPi;
    float py=0.5+latitude/c_pi;

    vec3 normal=vec3(x,y,z);
    float illumination=0.2+0.8*max(0.0,dot(light,normal));

    result=illumination*tophysical(texture(source,vec2(px,py)));
  }

  return result;
}

void main() {

  vec4 acc=vec4(0.0);

  for (int sy=0;sy<MULTISAMPLING;sy++) {
    float dy=((float(sy)+0.5)/MULTISAMPLING-0.5)*ubuf.delta.y;
    float y=2.0*ubuf.scale*(qt_TexCoord0.y+dy-0.5);
    for (int sx=0;sx<MULTISAMPLING;sx++) {
      float dx=((float(sx)+0.5)/MULTISAMPLING-0.5)*ubuf.delta.x;
      float x=2.0*ubuf.scale*(ubuf.delta.y/ubuf.delta.x)*(qt_TexCoord0.x+dx-0.5);
      acc+=trace(x,y);
    }
  }

  fragColor=ubuf.qt_Opacity*toperceptual(acc/(MULTISAMPLING*MULTISAMPLING));
}
