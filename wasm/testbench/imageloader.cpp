#include "imageloader.h"

#include <QNetworkReply>

ImageLoader::ImageLoader()
  :_network(this)
{
  connect(&_network,&QNetworkAccessManager::finished,this,&ImageLoader::replyFinished);
}

ImageLoader::~ImageLoader() {
}

const QImage& ImageLoader::image() const {
  return _image;
}

void ImageLoader::setImage(const QImage& img) {
  _image=img;
  emit imageChanged(img);
}

const QUrl& ImageLoader::src() const {
  return _src;
}

void ImageLoader::setSrc(const QUrl& url) {
  if (url!=src()) {
    _src=url;
    emit srcChanged(src());

    QNetworkRequest req(url);
    req.setRawHeader(QByteArray("Access-Control-Allow-Origin"),QByteArray("*"));
    _network.get(req);
  }
}

void ImageLoader::replyFinished(QNetworkReply* reply) {
  qInfo() << "ImageLoader got reply from" << reply->url();
  QImage img;
  const QByteArray bytes=reply->readAll();
  img.loadFromData(bytes,0);
  setImage(img);
  reply->deleteLater();
  qInfo() << "ImageLoader got" << bytes.size() << "bytes";
}
