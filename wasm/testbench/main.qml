import QtQuick

import Testbench

Window {
  id: window
  width: 800
  height: 600
  visible: true
  color: '#000000'

//  ImageLoader {
//    id: imageloader
//    //src: 'http://gtxweb.timday.tech/data/satmap/satmap-v6-2-8km.jpg'  // There's also lower resolution satmap-v6-2-16km.jpg
//    src: 'http://gtxweb.s3.eu-west-1.amazonaws.com/data/satmap/satmap-v6-2-8km.jpg'
//  }

  Timer {
    id: timesequenceAnimation
    interval: 100
    repeat: true
    running: true
    onTriggered: frame+=1

    property int frame: 0
  }

  Image {
    id: texture0
    visible: false
    //source: 'http://gtxweb.timday.tech/data/satmap/satmap-v6-2-8km.jpg'
    source: 'file:/home/local/project/gtx/CciContent/data/GHGs/CO2/200301.jpg'
    mipmap: false
  }

  Image {
    id: texture1
    visible: false
    //source: 'http://gtxweb.timday.tech/data/satmap/satmap-v6-2-8km.jpg'
    source: 'file:/home/local/project/gtx/CciContent/data/GHGs/CO2/201312.jpg'
    mipmap: false
  }

  ShaderEffectSource {
    id: fxsrc
    live: true
    sourceItem: (timesequenceAnimation.frame%2==0 ? texture0 : texture1)
    //wrapMode: ShaderEffectSource.Repeat  // Seems to be only supported for power-of-two textures in WebGL
  }

  ShaderEffect {
    anchors.fill: parent
    property variant source: fxsrc
    property variant delta: Qt.size(1.0/width,1.0/height)
    property variant scale: 1.25
    property variant rotation: 0.0
    fragmentShader: "globe.frag.qsb"

    MouseArea {
      anchors.fill: parent
      cursorShape: (pressed ? Qt.ClosedHandCursor : Qt.OpenHandCursor)

      onPressed: (mouse)=>{ox=mouse.x;oy=mouse.y;mouse.accepted=true;}
      onPositionChanged: (mouse)=>{parent.rotation+=parent.scale*0.005*(mouse.x-ox);ox=mouse.x;oy=mouse.y;}
      onWheel: (wheel)=>{parent.scale*=Math.exp(-wheel.angleDelta.y*0.001);wheel.accepted=true;}

      property real ox: 0.0
      property real oy: 0.0
    }

    function glInfo(info) {
      var shaderType = "unknown";
  	  if (info.profile === GraphicsInfo.OpenGLCoreProfile) {
  	    shaderType = "gl33";
  	  } else {
  	    if (info.majorVersion >= 3) {
  	      shaderType = "es30";
        } else {
  	      shaderType = "es20";
  	    }
      }

      var renderabledict={}
      renderabledict[GraphicsInfo.SurfaceFormatUnspecified]="Unspecified renderable type"
      renderabledict[GraphicsInfo.SurfaceFormatOpenGL]="OpenGL"
      renderabledict[GraphicsInfo.SurfaceFormatOpenGLES]="OpenGLES"

      var apidict={}
      apidict[GraphicsInfo.Unknown]="Unknown"
      apidict[GraphicsInfo.Software]="Software"
      apidict[GraphicsInfo.OpenGL]="OpenGL or OpenGL ES"
      apidict[GraphicsInfo.Direct3D12]="Direct3D 12"
      apidict[GraphicsInfo.OpenVG]="OpenVG"
      apidict[GraphicsInfo.OpenGLRhi]="OpenGL on top of QRhi"
      apidict[GraphicsInfo.Direct3D11Rhi]="Direct3D 11 on top of QRhi"
      apidict[GraphicsInfo.VulkanRhi]="Vulkan on top of QRhi"
      apidict[GraphicsInfo.MetalRhi]="Metal on top of QRhi"
      apidict[GraphicsInfo.NullRhi]="Null (no output) on top of QRhi"

      var profiledict={}
      profiledict[GraphicsInfo.OpenGLNoProfile]="None"
      profiledict[GraphicsInfo.OpenGLCoreProfile]="Core"
      profiledict[GraphicsInfo.OpenGLOpenGLCompatibilityProfile]="NoneCompatability"
      
  	  var text = "%4  Version: %1.%2  Profile: %3  API: %5".arg(
        info.majorVersion
      ).arg(
        info.minorVersion
      ).arg(
  	    profiledict[info.profile]
      ).arg(
        renderabledict[info.renderableType]
      ).arg(
        apidict[info.api]
      );
      
  	  console.log("Graphics: %1".arg(text));
  	  console.log("Shader type: %1".arg(shaderType));
    }

    Component.onCompleted: {
      glInfo(GraphicsInfo);
    }
  }
}
