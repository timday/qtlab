OMG big changes in Qt6.

Need to use qsb tool (included, at least in native branch) to build .qsb portable files from .vert and .frag
https://doc.qt.io/qt-6/qtshadertools-build.html

Shader effect might not appear until 6.3... (no earlier doc than qt6-dev)
  https://doc-snapshots.qt.io/qt6-dev/qml-qtquick-shadereffect.html
So... that might rule out shader-style rendering until 6.3.  Might have to figure out quick3d's support.
But is in the examples.

----

Running with bleeding edge browsers

Firefox and Chromium installed from flathub:
    flatpak install flathub org.mozilla.firefox
    flatpak install flathub org.chromium.Chromium

Run with:
    flatpak run org.mozilla.firefox
    flatpak run org.chromium.Chromium

(Probably can't run flatpak Chromium until upgrade from Buster.  See https://github.com/flathub/org.chromium.Chromium/issues/100 ).

In ShaderEffectSource, `wrapMode: ShaderEffectSource.Repeat` seems problematic.
    Firefox 93.0: `WebGL warning: drawElementsInstanced: TEXTURE_2D at unit 1 is incomplete: Non-power-of-two textures must have a wrap mode of CLAMP_TO_EDGE.`    
    Chromium 90.0.4430.212 (in buster, bullseye has same): [.WebGL-0x563b5b67e050]RENDER WARNING: texture bound to texture unit 1 is not renderable. It might be non-power-of-2 or have incompatible texture filtering (maybe)?

But... don't think I ever checked whether the javascript version implements it anyway.
