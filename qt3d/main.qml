import QtQuick 2.15

import QtQuick.Controls 2.15
import QtQuick.Controls.Universal 2.15
import QtQuick.Layouts 1.15

import QtQuick3D 1.15 

Rectangle {
  id: main

  width: 800
  height: 600
 
  color: 'black'

  Universal.theme: Universal.Dark
  Universal.accent: '#444444'

  Node {
    id: scene
    DirectionalLight {
      ambientColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    }
    Model {
      source: "#Cube"
      y: -104
      scale: Qt.vector3d(3, 3, 0.1)
      eulerRotation.x: -90
      materials: [
        DefaultMaterial {
          diffuseColor: Qt.rgba(0.8, 0.0, 0.0, 1.0)
        }
      ]
      PropertyAnimation on eulerRotation.y {
        loops: Animation.Infinite
        duration: 5000
        to: 0
        from: -360
      }
    }
    OrthographicCamera {
      id: camera
      z: 600
    }
  }

  View3D {
    anchors.fill: parent
    importScene: scene
    camera: camera
  }

  Text {
    text: 'Hello'
    color: '#ffff00'
  }
}

// Useful: https://doc.qt.io/qt-5/qtquick3d-view3d-main-qml.html
// https://stackoverflow.com/questions/59589839/add-qml-item-to-qt3dquickwindow
// Interesting: https://www.kdab.com/wp-content/uploads/stories/QtWS_Earth_Rendering_with_Qt3D.pdf

