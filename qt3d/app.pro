TEMPLATE = app

CONFIG += release
CONFIG += qtquickcompiler

QT += core gui quick qml quickcontrols2 # 3dcore 3drender 3dinput 3dlogic 3dextras 3dquick 3danimation

TARGET = app

SOURCES += main.cpp

RESOURCES += main.qrc

cache()
