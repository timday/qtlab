#include <QGuiApplication>
#include <QQuickView>
#include <QQuickStyle>

int main(int argc,char* argv[]) {

  QGuiApplication app(argc,argv);

  QQuickStyle::setStyle("Universal");
  
  QQuickView viewer;
  viewer.setResizeMode(QQuickView::SizeRootObjectToView);
  viewer.resize(QSize(800,600));
  viewer.setSource(QUrl("qrc:///main.qml"));
  viewer.show();
  
  return app.exec();
}
