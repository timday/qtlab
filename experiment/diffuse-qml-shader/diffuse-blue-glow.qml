import QtQuick 2.2

Item {
  width: 660
  height: 480
  scale: 1.0
  smooth: false
  //layer.enabled: true
  //layer.smooth: false

  Rectangle {
    id: main
    anchors.fill: parent
    color: '#000000'

    // Set up a initial pattern
    Text {
      anchors.centerIn: parent
      font.family: "Droid sans"
      horizontalAlignment: Text.AlignHCenter
      text: "Magic Theatre.\nEntrance\nnot for\neverybody."
      font.pixelSize: 80
      color: '#ffffff'
      smooth: false
    }
  }

  ShaderEffectSource {
    id: base
    anchors.fill: parent
    live: false
    recursive: true
    sourceItem: main
    hideSource: true
  }

  ShaderEffectSource {
    id: buffer
    anchors.fill: parent
    live: true  // Goes flat-out
    recursive: true
    sourceItem: buffer

    ShaderEffect {
      id: diffuse
      anchors.fill: parent
      property variant src0: base
      property variant src1: buffer

      property real dx: 1.0/main.width
      property real dy: 1.0/main.height

      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"

      fragmentShader: "
        uniform highp float dx;
        uniform highp float dy;
        varying highp vec2 coord;
        uniform sampler2D src0;
        uniform sampler2D src1;
        void main() {
          highp vec4 tie=texture2D(src0,coord);
            
          highp vec4 v11=texture2D(src1,coord);
          highp vec4 v00=texture2D(src1,coord+vec2(-dx,-dy));
          highp vec4 v01=texture2D(src1,coord+vec2(0.0,-dy));
          highp vec4 v02=texture2D(src1,coord+vec2( dx,-dy));
          highp vec4 v10=texture2D(src1,coord+vec2(-dx,0.0));
          highp vec4 v12=texture2D(src1,coord+vec2( dx,0.0));
          highp vec4 v20=texture2D(src1,coord+vec2(-dx, dy));
          highp vec4 v21=texture2D(src1,coord+vec2(0.0, dy));
          highp vec4 v22=texture2D(src1,coord+vec2( dx, dy));

	  highp vec4 diffuse=0.05*(v00+v02+v20+v22)+0.2*(v01+v21+v10+v12)-v11;

	  highp vec4 nudge=tie.r*(tie-v11);

	  highp vec4 dst=v11+vec4(0.0,0.0,diffuse.b,0.0)+0.1*nudge;

	  // Create a 'sink' around border
	  if (coord.x<dx || coord.x>1.0-dx || coord.y<dy || coord.y>1.0-dy) {
	    dst=vec4(0.0,0.0,0.0,0.0);
	  }
	  
	  highp float k=0.125/255.0;

	  gl_FragColor=vec4(dst.r+k,dst.g+k,dst.b+k,1.0);
        }"
    }
  }
}
