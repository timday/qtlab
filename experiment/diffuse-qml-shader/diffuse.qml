import QtQuick 2.2

import QtQuick.Controls 1.2

Item {
  width: 640
  height: 480
  scale: 1.0
  smooth: false
  //layer.enabled: true
  //layer.smooth: false

  Rectangle {
    id: main
    anchors.fill: parent
    color: '#00ff00'

    // Set up a initial pattern
    Text {
      anchors.top: main.top
      anchors.horizontalCenter: main.horizontalCenter
      anchors.margins: 12
      font.family: "Sans"
      horizontalAlignment: Text.AlignHCenter
      text: "Magic Theatre"
      font.pixelSize: 80
      color: '#ffff00'
      smooth: false
    }

    Text {
      anchors.centerIn: parent
      font.family: "Sans"
      horizontalAlignment: Text.AlignHCenter
      text: "Entrance\nnot for\neverybody."
      font.pixelSize: 72
      color: '#ffff00'
      smooth: false
    }

    Button {
      anchors.bottom: main.bottom
      anchors.left: main.left
      anchors.margins: 24
      text: "Enter..."
      tooltip: "Starts reaction-diffusion system"
      onClicked: {console.log('Enter');buffer.running=true;}
    }

    Button {
      anchors.bottom: main.bottom
      anchors.horizontalCenter: main.horizontalCenter
      anchors.margins: 24
      text: "...for madmen only..."
      tooltip: "Starts reaction-diffusion system"
      onClicked: {console.log('madmen');buffer.running=true;}
    }

    Button {
      anchors.bottom: main.bottom
      anchors.right: main.right
      anchors.margins: 24
      text: "Leave..."
      tooltip: "Reset to original"
      onClicked: {console.log('Leave');buffer.running=false;}
    }
  }

  // Initial shader source to bootstrap buffer
  ShaderEffectSource {
    id: base
    anchors.fill: parent
    live: false
    recursive: false
    sourceItem: main
    hideSource: true
  }

  // Iterative shader running a reaction-diffusion system
  ShaderEffectSource {
    id: buffer
    anchors.fill: parent
    live: false  // Don't run flat-out; controlled using ticker
    recursive: true
    sourceItem: buffer

    property alias running: diffuse.running

    onRunningChanged: buffer.scheduleUpdate();  // Make sure a copy of base gets through

    Timer {
      id: ticker;
      interval: 1000/60;
      running: buffer.running
      repeat: true;
      onTriggered: {
        buffer.scheduleUpdate();
        if (diffuse.p<0.5) diffuse.p=1.0-diffuse.p;else diffuse.p=0.5*Math.random(); // Avoid long-term bias
      }
    }

    ShaderEffect {
      id: diffuse
      anchors.fill: parent

      // Shader will just pass-through unless set
      property bool running: false
  
      property variant src: (running ? buffer : base)

      property real dx: 1.0/main.width
      property real dy: 1.0/main.height

      property real f: 0.0545  // 'f' rate for reaction-diffusion system
      property real k: 0.062   // 'k' rate for reaction-diffusion system
      property real p: 0.5     // Random number to jitter rounding (avoids buffer quantization issues)

      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"

      fragmentShader: "
        uniform bool running;
        uniform highp float dx;
        uniform highp float dy;
        uniform highp float f;
        uniform highp float k;
        uniform highp float p;
        varying highp vec2 coord;
        uniform sampler2D src;
        void main() {

          highp vec4 v11=texture2D(src,coord);            

	  if (running) {
            highp vec4 v00=texture2D(src,coord+vec2(-dx,-dy));
            highp vec4 v01=texture2D(src,coord+vec2(0.0,-dy));
            highp vec4 v02=texture2D(src,coord+vec2( dx,-dy));
            highp vec4 v10=texture2D(src,coord+vec2(-dx,0.0));
            highp vec4 v12=texture2D(src,coord+vec2( dx,0.0));
            highp vec4 v20=texture2D(src,coord+vec2(-dx, dy));
            highp vec4 v21=texture2D(src,coord+vec2(0.0, dy));
            highp vec4 v22=texture2D(src,coord+vec2( dx, dy));

            // See diffuse.py for model... 'g' will be the food, 'r' the consumer density

	    highp vec4 diffusion=0.05*(v00+v02+v20+v22)+0.2*(v01+v21+v10+v12)-v11;
	    highp float reaction=v11.g*v11.r*v11.r;
  	    highp float feed=f*(1.0-v11.g);
	    highp float kill=(f+k)*v11.r;

	    highp float dr=(0.5*diffusion.r+reaction-kill);
	    highp float dg=(    diffusion.g-reaction+feed);

	    highp vec4 dst=vec4(
	      v11.r+dr,
              v11.g+dg,
              0.0,
	      1.0
	    );

	    dst.r=floor(255.0*dst.r+p)/255.0;
	    dst.g=floor(255.0*dst.g+p)/255.0;

	    // Create a 'sink' around border
	    if (coord.x<dx || coord.x>1.0-dx || coord.y<dy || coord.y>1.0-dy) {
	      dst=vec4(0.0,0.0,0.0,1.0);
	    }
	  
	    gl_FragColor=dst;
          } else {
            gl_FragColor=v11;
          }
        }"
    }
  }

  // Recolour the buffer
  ShaderEffectSource {
    id: recolor
    anchors.fill: parent
    live: false  // Don't run flat-out
    sourceItem: buffer
    hideSource: true

    ShaderEffect {
      anchors.fill: parent
    
      property variant src: parent.sourceItem
    
      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"
    
      fragmentShader: "
        varying highp vec2 coord;
        uniform sampler2D src;
        void main() {
          highp vec4 v=texture2D(src,coord);
          gl_FragColor=vec4(2.0*v.r,v.r,0.0,1.0);
        }"
    }
  }
}
