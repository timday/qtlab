#!/usr/bin/env ipython

# Implementation of http://www.karlsims.com/rd.html

import cairo
import Image
import math
import matplotlib.colors
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pango
import pangocairo

def advance(s,f,k,dt):
    h,w,=s.shape[0:2]
    
    s11=s[1:h-1,1:w-1,:]
    
    s01=s[0:h-2,1:w-1,:]
    s21=s[2:h  ,1:w-1,:]
    s10=s[1:h-1,0:w-2,:]
    s12=s[1:h-1,2:w  ,:]
 
    s00=s[0:h-2,0:w-2,:]
    s02=s[0:h-2,2:w  ,:]
    s20=s[2:h  ,0:w-2,:]
    s22=s[2:h  ,2:w  ,:]
    
    diffusion=0.05*(s00+s02+s20+s22)+0.2*(s01+s21+s10+s12)-s11
    reaction=s11[:,:,0]*s11[:,:,1]*s11[:,:,1]
    feed=f*(1.0-s11[:,:,0])
    kill=(f+k)*s11[:,:,1]
    
    s[1:h-1,1:w-1,0]+=dt*(    diffusion[:,:,0]-reaction+feed)
    s[1:h-1,1:w-1,1]+=dt*(0.5*diffusion[:,:,1]+reaction-kill)

n=0
s=np.zeros((480,640,2),dtype=np.float32)
s[1:-1,1:-1,0]=1.0
#s[s.shape[0]/2-1:s.shape[0]/2+1,s.shape[1]/2-5:s.shape[1]/2+5,1]=1.0

# to see font family names:
font_map = pangocairo.cairo_font_map_get_default()
families = font_map.list_families()
print [f.get_name() for f in font_map.list_families()]

surface=cairo.ImageSurface(cairo.FORMAT_A8,s.shape[1],s.shape[0])
ctx = cairo.Context(surface)
ctx.set_source_rgb(1.0,0.0,0.0)

#msg='T  o  s  h  i  b  a\n\nM  e  d  i  c  a  l\n\nV i s u a l i z a t i o n\n\nS  y  s  t  e  m  s'
#msg='R  e  a  c  t  i  o  n\n\nD  i  f  f  u  s  i  o  n\n\nS  y  s  t  e  m  s'
msg="That's a nasty rash\n\nyou've got there;\n\nI'd get that looked\n\nat if I were you!"

# http://fishbowl.pastiche.org/2004/12/14/war_of_the_worlds/
#msg="...as someone with a\n\nmicroscope studies\n\ncreatures that swarm\n\nand multiply...."

if True:
    pctx=pangocairo.CairoContext(ctx)
    pctx.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
    layout = pctx.create_layout()
    fontname = 'Inconsolata' # 'Droid Serif' # 'Courier New' # 'Sans' # 'Serif' # 'Sans'
    font = pango.FontDescription(fontname + " 40")
    layout.set_font_description(font)
    layout.set_text(msg)
    layout.set_alignment(pango.ALIGN_CENTER)
    ink,ext=layout.get_pixel_extents()
    ctx.translate((s.shape[1]-ext[2])/2,(s.shape[0]-ext[3])/2)
    pctx.update_layout(layout)
    pctx.show_layout(layout)
else:
    ctx.select_font_face("Sans",cairo.FONT_SLANT_NORMAL,cairo.FONT_WEIGHT_NORMAL)
    ctx.set_font_size(48)
    ctx.new_path()
    txtxbearing, txtybearing, txtwidth, txtheight, txtxadvance, txtyadvance = ctx.text_extents(msg)
    ctx.move_to((s.shape[1]-txtwidth)/2,(s.shape[0]-txtheight)/2+txtheight)
    ctx.text_path(msg)
    ctx.fill()

txt=Image.frombuffer("L",(surface.get_width(),surface.get_height() ),surface.get_data(),"raw","L",0,1)
s[:,:,1]=0.25*np.float32(np.asarray(txt))/255.0
plt.imshow(s[:,:,1],cmap=plt.gray())
plt.show()

colordict={
    'red':((0.0,1.0,1.0),(1.0,1.0,1.0)),
    'green':((0.0,1.0,1.0),(1.0,0.0,0.0)),
    'blue':((0.0,1.0,1.0),(1.0,0.0,0.0))
    }
my_cmap=matplotlib.colors.LinearSegmentedColormap('my_cmap',colordict,256)

fig = plt.figure()
im=plt.imshow(np.sqrt(s[:,:,1]),vmin=0.0,vmax=0.5,cmap=my_cmap)
plt.gca().set_axis_off()
#plt.colorbar()

def updatefig(*args):
    global s
    global n

    for i in xrange(1+n/100):
        advance(s,0.0367,0.0649,1.0)  # Mitosis example parameters
        #advance(s,0.0545,0.062,1.0)  # Coral example parameters
        n+=1

    im.set_array(s[:,:,1])
    plt.title("t = {0}".format(n),family='monospace')
    return im

ani=animation.FuncAnimation(fig,updatefig,interval=1,blit=False,frames=900)

# Use one of:
ani.save('animation.mp4',fps=30) #,clear_temp=False,codec='mpeg4'
#plt.show()

