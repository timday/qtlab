import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Particles 2.0

Item {
  width: 640
  height: 480

  Item {
    id: main
    anchors.fill: parent

    ParticleSystem {
      anchors.fill: parent

      ImageParticle {
        source: "qrc:///particleresources/fuzzydot.png"
        entryEffect: ImageParticle.None
      }

      Emitter {
        enabled: rain.checked
        anchors.fill: parent
        emitRate: rainrate.value
        lifeSpan: 100
      }
    }

    ParticleSystem {
      anchors.fill: parent

      ItemParticle {
        delegate: Rectangle {color: '#000000';width:5;height:5;radius:2}
        fade: false
      }

      Wander {
        pace: 300
        affectedParameter: Wander.Velocity
        xVariance: main.width
        yVariance: main.height
      }

      Emitter {
        enabled: bugs.checked
        emitRate: bugcount.value
        anchors.fill: parent
        maximumEmitted: bugcount.value 
        lifeSpan: 5000
        lifeSpanVariation: 4000
      }
    }

    Text {
      id: title
      anchors.top: main.top
      anchors.left: main.left
      anchors.margins: 12
      textFormat: Text.StyledText
      horizontalAlignment: Text.AlignLeft
      font.pixelSize: 76
      font.family: "GoodDog"
      text: "Surface gravity waves<br/>in a Qt/QML + GLSL UI"
    }

    Text {
      id: marqueetext
      visible: scroll.checked
      anchors.top: title.bottom
      anchors.topMargin: 0
      x: main.width
      textFormat: Text.StyledText
      font.pixelSize: 48
      font.family: "GoodDog"
      text: "by timday"
      SequentialAnimation {
        running: scroll.checked
        NumberAnimation {
          target: marqueetext
          property: 'x'
          duration: 10000
          from: main.width
          to: 12
          easing.type: Easing.OutCubic
        }
      }
    }

    Rectangle {
      id: spot
      color: '#000000'
      x: -10
      y: -10
      width: 2
      height: 2
    }

    MouseArea {
      anchors.fill: parent
      hoverEnabled: true
      onPositionChanged: {spot.x=mouse.x;spot.y=mouse.y;}
      onPressed: {spot.x=mouse.x;spot.y=mouse.y;}
    }

    ColumnLayout {
      id: controls
      anchors.bottom: main.bottom
      anchors.left: main.left
      anchors.right: main.right
      anchors.margins: 24

      property int w: (main.width-2*anchors.margins)/4

      RowLayout {

        CheckBox {
          id: liquify
          text: "<b>Fluid</b>: "+(checked ? "<b>ON</b>" : "OFF")
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }
        
        CheckBox {
          id: scroll
          text: "<b>Credit</b>: "+(checked ? "<b>ON</b>" : "OFF")
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }

        Slider {
          id: bugcount
          visible: bugs.checked
          orientation: Qt.Horizontal
          minimumValue: 1
          maximumValue: 20
          stepSize: 1
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }

        Item {
          visible: !bugs.checked
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }

        CheckBox {
          id: bugs
          text: "<b>Bugs</b>"+(checked ? ": "+bugcount.value : "")
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }
      }

      RowLayout {

        Item {
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }
        
        CheckBox {
          id: skymap
          text: "<b>Skymap</b>: "+(checked ? "<b>ON</b>" : "OFF")
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }
            
        Slider {
          id: rainrate
          visible: rain.checked
          orientation: Qt.Horizontal
          minimumValue: 1
          maximumValue: 60
          stepSize: 1
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }

        Item {
          visible: !rain.checked
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }

        CheckBox {
          id: rain
          text: "<b>Rain</b>"+(checked ? ": "+rainrate.value+"/s" : "")
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }        
      }

      RowLayout {

        Item {
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }
        
        Item {
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }
            
        Slider {
          id: viscosity
          orientation: Qt.Horizontal
          minimumValue: 0
          maximumValue: 200
          stepSize: 1
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }

        Text {
          text: "<b>Viscosity</b>: "+waves.ks.toFixed(4)
          Layout.fillWidth: true
          Layout.minimumWidth: controls.w
          Layout.maximumWidth: controls.w
        }        
      }
    }
  }

  ShaderEffectSource {
    id: base
    anchors.fill: parent
    live: true
    recursive: false
    sourceItem: main
    hideSource: true
  }

  ShaderEffectSource {
    id: buffer
    anchors.fill: parent
    live: false
    recursive: true
    sourceItem: buffer
    wrapMode: ShaderEffectSource.ClampToEdge
    smooth: false
    
    Timer {
      id: ticker
      interval: 1000/60
      running: true
      repeat: true
      onTriggered: {
        buffer.scheduleUpdate();
        if (waves.p<0.5) waves.p=1.0-waves.p;else waves.p=0.5*Math.random(); // Avoid long-term bias
      }
    }

    ShaderEffect {
      id: waves
      anchors.fill: parent

      blending: false // Use all of RGBA for physical state

      property variant src0: buffer
      property variant src1: base

      property bool running: liquify.checked

      property real dx: 1.0/buffer.width
      property real dy: 1.0/buffer.height
      property real bias: (127.0*256.0)/65535.0  // Zero point for floats saved to 2x8bit channels

      property real c:  0.5   // Speed of light for waves
      property real dt: 0.5   // Timestep
      property real ks: Math.pow(0.1,1.0+(200-viscosity.value)/100.0)   // Smoothing/viscosity
      property real ka: 0.001 // Attentuation/damping
      property real p:  0.5   // Random number to jitter rounding (avoids buffer quantization issues)

      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"

      fragmentShader: "
        uniform bool running;
        uniform highp float dx;
        uniform highp float dy;
        uniform highp float bias;
        uniform highp float c;
        uniform highp float dt;
        uniform highp float ks;
        uniform highp float ka;
        uniform highp float p;
        varying highp vec2 coord;
        uniform sampler2D src0;
        uniform sampler2D src1;

        // u,v nominally in range -0.5 to +0.5
	// Need to be able to represent small numbers more accurately
        // than straight quantization to 8 bit permits, and reliably
        // preserve them without bias.
        // To recover from 2 8 bit channels::
        //   (((uint)(255.0*hi)<<8)+((uint)(255.0*lo))/65535.0-bias
        // To store:
        //   value=max(0.0,min(v+bias,1.0))
        //   value=floor(65535.0*value)+p);
        //   hi=floor(value/256.0)/255.0;
	//   lo=mod(value,256.0)/255.0;

	vec2 load(vec2 x) {
          highp vec4 s=texture2D(src0,x);
	  highp vec2 hi=s.rb;
	  highp vec2 lo=s.ga;
          return (floor(255.0*hi)*256.0+floor(255.0*lo))/65535.0-bias;
        }

        void main() {

          highp vec4 m=texture2D(src1,coord);

          if (!running || m.a>0.0) {
            gl_FragColor=vec4(0.5+0.5*m.a*(0.5+0.5*m.g),0.0,bias,0.0);
          } else {

            highp vec2 s=load(coord);

            highp vec2 sa0=load(coord+vec2(-dx,0.0));
	    highp vec2 sa1=load(coord+vec2( dx,0.0));
            highp vec2 sa2=load(coord+vec2(0.0,-dy));
            highp vec2 sa3=load(coord+vec2(0.0, dy));

            highp vec2 sa4=load(coord+vec2(-dx,-dy));
	    highp vec2 sa5=load(coord+vec2( dx,-dy));
            highp vec2 sa6=load(coord+vec2(-dx, dy));
            highp vec2 sa7=load(coord+vec2( dx, dy));
	    
            highp vec2 sa=sa0+sa1+sa2+sa3;
            highp vec2 so=sa4+sa5+sa6+sa7;
            
            highp float dv=(dt*c*c)*(sa.r-4.0*s.r);

            // Smoothing
            s=(1.0-ks)*s+ks*(sa+so)/8.0;

            // Attenuation
            s=(1.0-ka)*s;
                        
            highp float newv;
            highp float newu;
            if (coord.x<dx || coord.x>1.0-dx || coord.y<dy || coord.y>1.0-dy) {
              newv=0.0;
              newu=0.0;
            } else {
              newv=s.g+dv;
              newu=s.r+dt*newv;
            }

            newv=max(0.0,min(bias+newv,1.0));
            newu=max(0.0,min(bias+newu,1.0));

            if (newu==0.0 || newu==1.0) {newv==0.0;}
            
            highp vec2 value=floor(65535.0*vec2(newu,newv)+p);
            highp vec2 hi=floor(value/256.0)/255.0;
	    highp vec2 lo=mod(value,256.0)/255.0;
            
            gl_FragColor=vec4(hi.r,lo.r,hi.g,lo.g);
            
          }
        }"
    }
  }

  // Shade the buffer height field
  ShaderEffectSource {
    id: recolor
    anchors.fill: parent
    live: true
    sourceItem: buffer
    hideSource: true

    ShaderEffect {
      anchors.fill: parent

      blending: false  // All output alpha=1.0
    
      property real dx: 1.0/buffer.width
      property real dy: 1.0/buffer.height

      Image {
        id: skymapimage
        visible: false
        source: "skymap.jpg"
        smooth: true
      }

      property variant src0: parent.sourceItem
      property variant src1: base
      property variant src2: skymapimage
      property bool useskymap: skymap.checked
    
      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"
    
      fragmentShader: "
        uniform highp float dx;
        uniform highp float dy;
        varying highp vec2 coord;
        uniform sampler2D src0;
        uniform sampler2D src1;
        uniform sampler2D src2;
        uniform bool useskymap;

	highp float load(vec2 pos) {
          highp vec2 s=texture2D(src0,pos).rg;
          return (floor(255.0*s.r)*256.0+floor(255.0*s.g))/65535.0;
        }

        vec4 over(vec4 front,vec4 back) {
          return front.a*front+(1.0-front.a)*back;
        }

        void main() {

          highp float ha0=load(coord+vec2(-dx,0.0));
          highp float ha1=load(coord+vec2( dx,0.0));
          highp float ha2=load(coord+vec2(0.0,-dy));
          highp float ha3=load(coord+vec2(0.0, dy));
          
          highp vec3 g=vec3(4.0*(ha0-ha1),4.0*(ha2-ha3),1.0);
          highp vec3 n=normalize(g);
          
          if (useskymap) {
            highp vec3 i=vec3(0.0,0.0,1.0);
            highp vec3 r=i-2.0*dot(n,i)*n;
            highp float y=1.0-r.z;
            highp float ipi=1.0/3.1415926535897932384626433832795;
            highp float x=0.5*(1.0+ipi*atan(r.y,r.x));
            gl_FragColor=texture2D(src2,vec2(x,y));
          } else {
            highp vec3 l=normalize(vec3(3.0,-2.0,2.0));
            highp float d=dot(n,l);
            gl_FragColor=vec4(d,d,d,1.0);
          }

          highp vec4 m=texture2D(src1,coord);
          gl_FragColor=over(m,gl_FragColor);
        }"
    }
  }
}
