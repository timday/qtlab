#!/usr/bin/env ipython

# Wave equations
# From the 001.fluid.height_field_simulation.pdf document at https://web.archive.org/web/20150118045137/http://www.roxlu.com/downloads/scholar/
# (Also at https://ubm-twvideo01.s3.amazonaws.com/o1/vault/gdc08/slides/S6509i1.pdf )

import cairo
from PIL import Image
import math
import matplotlib.colors
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pango
import pangocairo

def smooth(s):
    t=np.zeros(s.shape,dtype=np.float32)
    w=0.0
    for r in xrange(0,3):
        for c in xrange(0,3):
            k=0.0025
            if r==1 and c==1: k=1.0
            t[1:s.shape[0]-1,1:s.shape[1]-1,:]+=k*s[r:s.shape[0]-2+r,c:s.shape[1]-2+c,:]
            w+=k
    s[:,:,:]=t/w

# s has 2 components
def advance(s,m):

    h,w,=s.shape[0:2]

    dt=0.5
    c=1.0

    u=s[:,:,0]
    dv=(dt*c*c)*(u[1:h-1,2:w]+u[1:h-1,0:w-2]+u[2:h,1:w-1]+u[0:h-2,1:w-1] - 4.0*u[1:h-1,1:w-1])

    s[1:h-1,1:w-1,1]+=dv
    s[1:h-1,1:w-1,0]+=dt*s[1:h-1,1:w-1,1]

    # Stabilize (basically, a bit of viscosity?)
    smooth(s)

    # Clamp fixed pixels
    s[:,:,0]=(1.0-m)*s[:,:,0]
    s[:,:,1]=(1.0-m)*s[:,:,1]
    
def render(s,m):
    h,w,=s.shape[0:2]
    v=s[:,:,0]+m
    dx=100.0*0.5*(v[1:h-1,2:w]-v[1:h-1,0:w-2])
    dy=100.0*0.5*(v[2:h,1:w-1]-v[0:h-2,1:w-1])
    dz=np.ones(dx.shape)
    m=np.sqrt(dx*dx+dy*dy+dz)
    
    i=0.5+0.5*(-3.0*dx+2.0*dy+1.0*dz)/(m*math.sqrt(3.0)*math.sqrt(9.0+4.0+1.0))
    return i

n=0
s=np.zeros((256,256,3),dtype=np.float32)
s[1:-1,1:-1,0]=0.0
#s[s.shape[0]/2-1:s.shape[0]/2+1,s.shape[1]/2-5:s.shape[1]/2+5,1]=1.0

# to see font family names:
#font_map = pangocairo.cairo_font_map_get_default()
#families = font_map.list_families()
#print [f.get_name() for f in font_map.list_families()]

surface=cairo.ImageSurface(cairo.FORMAT_A8,s.shape[1],s.shape[0])
ctx = cairo.Context(surface)
ctx.set_source_rgb(1.0,0.0,0.0)

#msg="\n\n\n\n\nLike as the waves\nmake towards the\npebbl'd shore, so do\nour minutes, hasten\nto their end."
#msg="\n\n\n\n\nLove the moment,\nand the energy of\nthat moment will spread\nbeyond all boundaries."
msg="O"

if True:
    # Pango text rendering; handles newlines
    pctx=pangocairo.CairoContext(ctx)
    pctx.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
    layout = pctx.create_layout()
    fontname = 'Inconsolata' # 'Droid Serif' # 'Courier New' # 'Sans' # 'Serif' # 'Sans'
    font = pango.FontDescription(fontname + " 14")
    layout.set_font_description(font)
    layout.set_text(msg)
    layout.set_alignment(pango.ALIGN_CENTER)
    ink,ext=layout.get_pixel_extents()
    ctx.translate((s.shape[1]-ext[2])/2,(s.shape[0]-ext[3])/2)
    pctx.update_layout(layout)
    pctx.show_layout(layout)
else:
    # Cairo text rendering; doesn't handle newlines
    ctx.select_font_face("Sans",cairo.FONT_SLANT_NORMAL,cairo.FONT_WEIGHT_NORMAL)
    ctx.set_font_size(48)
    ctx.new_path()
    txtxbearing, txtybearing, txtwidth, txtheight, txtxadvance, txtyadvance = ctx.text_extents(msg)
    ctx.move_to((s.shape[1]-txtwidth)/2,(s.shape[0]-txtheight)/2+txtheight)
    ctx.text_path(msg)
    ctx.fill()

txt=Image.frombuffer("L",(surface.get_width(),surface.get_height() ),surface.get_data(),"raw","L",0,1)
s[:,:,0]=np.float32(np.asarray(txt))/255.0
plt.imshow(s[:,:,0],cmap=plt.gray())
#plt.show()

# Mask for non-fluid pixels
m=np.zeros(s.shape[0:2])
m[50:100,50:100]=1.0
m[60:90,60:90]=0.0
m[70:80,90:100]=0.0
m[50:100,150:200]=1.0
#m[:1,:]=1.0
#m[-2:-1,:]=1.0
#m[:,:1]=1.0
#m[:,-2:-1]=1.0

# Fixed pixels will create an initial "pulse":
#s[:,:,0]+=m

fig = plt.figure()
im=plt.imshow(render(s,m),vmin=0.0,vmax=1.0,cmap=plt.gray())
plt.gca().set_axis_off()
#plt.colorbar()

def updatefig(*args):
    global s
    global n

    advance(s,m)
    n+=1

    im.set_array(render(s,m))
    plt.title("t = {0}, range {1:.3f} - {2:.3f} ".format(n,np.amin(s[:,:,0]),np.amax(s[:,:,0])),family='Courier New')
    return im

ani=animation.FuncAnimation(fig,updatefig,interval=1,blit=False,frames=1000)

# Use one of:
ani.save('animation.mp4',fps=25) #,clear_temp=False,codec='mpeg4'
#plt.show()

