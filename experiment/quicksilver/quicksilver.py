#!/usr/bin/env ipython

import cairo
import Image
import math
import matplotlib.colors
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pango
import pangocairo

# 3x3
def smooth(s,dt):
    k=0.01*dt
    t=np.zeros(s.shape,dtype=np.float32)
    for r in xrange(0,3):
        for c in xrange(0,3):
            if r==1 and c==1:
                m=1.0-8.0*k
            else:
                m=k
            t[1:s.shape[0]-1,1:s.shape[1]-1,:]+=m*s[r:s.shape[0]-2+r,c:s.shape[1]-2+c,:]
    s[:,:,:]=t

# s has 3 components
def advance(s):

    rows,cols=s.shape[0:2]

    dt=0.1
    g=1.0
    b=0.1
    H=1.0

    dx=0.5*(s[1:rows-1,2:,:]-s[1:rows-1,0:cols-2,:])
    dy=0.5*(s[2:,1:cols-1,:]-s[0:rows-2,1:cols-1,:])

    dudt=-g*dx[:,:,0]-b*s[1:rows-1,1:cols-1,1]
    dvdt=-g*dy[:,:,0]-b*s[1:rows-1,1:cols-1,2]

    # Surface tension?
    # Is a force in the direction of nearby negative curvature
    cx=np.zeros(s.shape[:2],dtype=np.float32)
    cx[1:rows-1,1:cols-1]=s[1:rows-1,2:,0]+s[1:rows-1,0:cols-2,0]-2.0*s[1:rows-1,1:cols-1,0]
    cy=np.zeros(s.shape[:2],dtype=np.float32)
    cy[1:rows-1,1:cols-1]=s[2:,1:cols-1,0]+s[0:rows-2,1:cols-1,0]-2.0*s[1:rows-1,1:cols-1,0]
    tx=cx[1:rows-1,2:]-cx[1:rows-1,0:cols-2]
    ty=cy[2:,1:cols-1]-cy[0:rows-2,1:cols-1]

    #dudt-=0.1*tx #/np.maximum(s[1:rows-1,1:cols-1,0],0.1)
    #dvdt-=0.1*ty #/np.maximum(s[1:rows-1,1:cols-1,0],0.1)

    # Have stuff flow away "downhill"
    #dvdt+=0.1

    dhdt=-H*(dx[:,:,1]+dy[:,:,2])

    s[1:rows-1,1:cols-1,0]+=dt*dhdt
    s[1:rows-1,1:cols-1,1]+=dt*dudt
    s[1:rows-1,1:cols-1,2]+=dt*dvdt

    # Clamp to zero height
    s[:,:,0]=np.maximum(s[:,:,0],np.zeros(s.shape[0:2]))
    s[:,:,1]=np.select([s[:,:,0]>0.0],[s[:,:,1]],0.0)
    s[:,:,2]=np.select([s[:,:,0]>0.0],[s[:,:,2]],0.0)
    
    # Stabilize (basically, a bit of viscosity?)
    smooth(s,dt)
    
    # Clamp edge flows?
    
def render(s):
    rows,cols=s.shape[0:2]
    h=s[:,:,0]
    dx=200.0*0.5*(h[1:rows-1,2:cols]-h[1:rows-1,0:cols-2])
    dy=200.0*0.5*(h[2:rows,1:cols-1]-h[0:rows-2,1:cols-1])
    dz=np.ones(dx.shape)
    m=np.sqrt(dx*dx+dy*dy+dz)
    i=0.5+0.5*(-3.0*dx+2.0*dy+1.0*dz)/(m*math.sqrt(3.0)*math.sqrt(9.0+4.0+1.0))
    #return np.select([h[1:rows-1,1:cols-1]>0.0],[i],0.0)
    return i

n=0
s=np.zeros((256,256,3),dtype=np.float32)

# to see font family names:
#font_map = pangocairo.cairo_font_map_get_default()
#families = font_map.list_families()
#print [f.get_name() for f in font_map.list_families()]

surface=cairo.ImageSurface(cairo.FORMAT_A8,s.shape[1],s.shape[0])
ctx = cairo.Context(surface)
ctx.set_source_rgb(1.0,0.0,0.0)

#msg="\n\n\n\n\nLike as the waves\nmake towards the\npebbl'd shore, so do\nour minutes, hasten\nto their end."
#msg="\n\n\n\n\nLove the moment,\nand the energy of\nthat moment will spread\nbeyond all boundaries."
msg="Liquid"

if True:
    # Pango text rendering; handles newlines
    pctx=pangocairo.CairoContext(ctx)
    pctx.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
    layout = pctx.create_layout()
    fontname = 'Inconsolata' # 'Droid Serif' # 'Courier New' # 'Sans' # 'Serif' # 'Sans'
    font = pango.FontDescription(fontname + " 48")
    layout.set_font_description(font)
    layout.set_text(msg)
    layout.set_alignment(pango.ALIGN_CENTER)
    ink,ext=layout.get_pixel_extents()
    ctx.translate((s.shape[1]-ext[2])/2,(s.shape[0]-ext[3])/2)
    pctx.update_layout(layout)
    pctx.show_layout(layout)
else:
    # Cairo text rendering; doesn't handle newlines
    ctx.select_font_face("Sans",cairo.FONT_SLANT_NORMAL,cairo.FONT_WEIGHT_NORMAL)
    ctx.set_font_size(48)
    ctx.new_path()
    txtxbearing, txtybearing, txtwidth, txtheight, txtxadvance, txtyadvance = ctx.text_extents(msg)
    ctx.move_to((s.shape[1]-txtwidth)/2,(s.shape[0]-txtheight)/2+txtheight)
    ctx.text_path(msg)
    ctx.fill()

txt=Image.frombuffer("L",(surface.get_width(),surface.get_height() ),surface.get_data(),"raw","L",0,1)
s[:,:,0]=np.float32(np.asarray(txt))/255.0
plt.imshow(s[:,:,0],cmap=plt.gray())
plt.show()

# Fixed pixels will create an initial "pulse":
#s[:,:,0]+=m

fig = plt.figure()
im=plt.imshow(render(s),vmin=0.0,vmax=1.0,cmap=plt.gray())
plt.gca().set_axis_off()
#plt.colorbar()

def updatefig(*args):
    global s
    global n

    advance(s)
    n+=1

    im.set_array(render(s))
    plt.title("i = {0}, range {1:.3f} - {2:.3f} ".format(n,np.amin(s[:,:,0]),np.amax(s[:,:,0])),family='Inconsolata')
    return im

ani=animation.FuncAnimation(fig,updatefig,interval=1,blit=False,frames=1000)

# Use one of:
#ani.save('animation.mp4',fps=25) #,clear_temp=False,codec='mpeg4'
plt.show()

