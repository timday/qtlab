import QtQuick 2.15
import QtCharts 2.15

// Don't think this is going to replace D3.js sunburst chart.
// Label on arm doesn't scale to large number of holdings.

ChartView {
  id: chart
  width: 800
  height: 600
  title: "ChartView Title"
  anchors.fill: parent
  legend.alignment: Qt.AlignBottom
  antialiasing: true
  
  dropShadowEnabled: true
  animationOptions: ChartView.AllAnimations
  theme: ChartView.ChartThemeLight

  readonly property real golden: (1.0+Math.sqrt(5.0))/2.0

  PieSeries {
    id: series0
    holeSize: size/golden
    PieSlice { labelVisible: true;label: "Volkswagen"; value: 13.5 }
    PieSlice { labelVisible: true;label: "Toyota"; value: 10.9 }
    PieSlice { labelVisible: true;label: "Ford"; value: 8.6 }
    PieSlice { labelVisible: true;label: "Skoda"; value: 8.2 }
    PieSlice { labelVisible: true;label: "Volvo"; value: 6.8 }
  }

  PieSeries {
    id: series1
    size: series0.holeSize
    holeSize: size/golden
    PieSlice { labelVisible: true;label: "VT"; value: 13.5+10.9;labelArmLengthFactor: 0.0 }
    PieSlice { labelVisible: true;label: "FSV"; value: 8.6+8.2+6.8;labelArmLengthFactor: 0.0}
  }
  
  /*Component.onCompleted: {
    // You can also manipulate slices dynamically, like append a slice or set a slice exploded
    var othersSlice = pieSeries.append("Others", 52.0);
    //pieSeries.find("Volkswagen").exploded = true;
  }*/
}

