import QtQuick 2.2

Item {
  width: 660
  height: 660
  scale: 3.0
  smooth: false
  layer.enabled: true
  layer.smooth: false

  Rectangle {
    id: main
    anchors.fill: parent
    color: '#000088'

    // Set up a initial pattern
    Item {
      anchors.centerIn: parent
      width: 3
      height: 3
      Rectangle {y:2;x:1;width:1;height:1;color:'#ffcc00';smooth:false;}
      Rectangle {y:1;x:0;width:1;height:1;color:'#ffcc00';smooth:false;}
      Rectangle {y:1;x:1;width:1;height:1;color:'#ffcc00';smooth:false;}
      Rectangle {y:0;x:1;width:1;height:1;color:'#ffcc00';smooth:false;}
      Rectangle {y:0;x:2;width:1;height:1;color:'#ffcc00';smooth:false;}
    }
  }

  // Use a recursive shader to repeatedly apply Game Of Life rules
  ShaderEffectSource {
    id: buffer
    anchors.fill: parent
    live: false  // Don't run "live"; is too fast, use timer to control updates instead
    recursive: true
    sourceItem: main

    // Need to bootstrap off initial image first, then switch to loopback
    Timer {
      interval: 100;
      running:true;
      onTriggered: {buffer.sourceItem=buffer;cgol.running=true;ticker.running=true;}
    }

    Timer {
      id: ticker;
      interval: 1000/30;
      running:false;
      repeat: true;
      onTriggered: {buffer.scheduleUpdate();}
    }

    ShaderEffect {
      id: cgol
      anchors.fill: parent
      property variant src: buffer

      property real dx: 1.0/main.width
      property real dy: 1.0/main.height
      property bool running: false

      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"

      fragmentShader: "
        uniform bool running;
        uniform highp float dx;
        uniform highp float dy;
        varying highp vec2 coord;
        uniform sampler2D src;
        void main() {
          bool alive=(texture2D(src,coord).r>0.5);
          int neighbours=0;
          for (int y=-1;y<=1;y++) {
            for (int x=-1;x<=1;x++) {
              if (x!=0 || y!=0) {
                highp vec2 p=coord+vec2(float(x)*dx,float(y)*dy);
                lowp vec4 tex=texture2D(src,p);
                if (tex.r>0.5) neighbours++;
              }
            }
          }
          if (running && (neighbours==3 || (alive && neighbours==2)) || (!running && alive)) {
            gl_FragColor = vec4(1.0,0.8,0.0,1.0);
          } else {
            gl_FragColor = vec4(0.0,0.0,0.533,1.0);
	  }
        }"
    }
  }
}
