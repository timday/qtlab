#ifndef _myqmlviewer_h_
#define _myqmlviewer_h_

#include <QQuickView>
#include <QString>

class MyQMLViewer : public QQuickView {
  Q_OBJECT
 public:
  MyQMLViewer();
  ~MyQMLViewer();

  Q_INVOKABLE void printToPDF(const QString&) const;
};

#endif
