import QtQuick 2.0

Rectangle {
  color: "#ffffff"

  Rectangle {
    x: parent.width/4
    y: parent.height/4
    width: parent.width/2
    height: parent.height/2
    color: "#0000ff"

    Rectangle {
      x: parent.width/8
      y: parent.height/8
      width: parent.width
      height: parent.height
      color: "#000000"
      opacity: 0.5
      z: -1
    }
  }

  Text {
    anchors.bottom: parent.bottom
    anchors.horizontalCenter: parent.horizontalCenter
    text: "Print to PDF"
    color: "#ff0000"	
    font.weight: Font.Bold
    
    MouseArea {
      anchors.fill: parent
      onClicked: viewer.printToPDF("q.pdf");
    }
  }
}
