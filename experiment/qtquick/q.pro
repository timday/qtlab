CONFIG += release

QT += core gui opengl quick printsupport

TARGET = q
TEMPLATE = app

SOURCES += $$system(ls *.cpp)
HEADERS += $$system(ls *.h)

DESTDIR = build
OBJECTS_DIR = build/.obj
MOC_DIR = build/.moc
RCC_DIR = build/.qrc
UI_DIR = build/.ui

LIBS += -lGLU

