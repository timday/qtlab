#include "MyQMLViewer.h"

#include <QQmlContext>
#include <QtDebug>
#include <QtPrintSupport>

MyQMLViewer::MyQMLViewer() {
  setResizeMode(QQuickView::SizeRootObjectToView);
  rootContext()->setContextProperty("viewer",this);
}

MyQMLViewer::~MyQMLViewer() {}

void MyQMLViewer::printToPDF(const QString& filename) const {
  qDebug() << "Printing PDF to " << filename;
  QPrinter printer(QPrinter::HighResolution);
  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPageSize(QPrinter::A4);
  printer.setOutputFileName(filename);
  printer.setOrientation(QPrinter::Landscape);
  QPainter painter(&printer);
  render(&painter);
  qDebug() << "Printing PDF done";
}
