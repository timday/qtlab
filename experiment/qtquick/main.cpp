#include <QApplication>
#include <QtCore>

#include "MyQMLViewer.h"

int main(int argc,char* argv[])
{
  QApplication app(argc,argv);

  MyQMLViewer viewer;
  viewer.setSource(QUrl::fromLocalFile("q.qml"));
  viewer.show();

  return app.exec();
}
