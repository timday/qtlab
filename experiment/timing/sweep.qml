import QtQuick 2.2
import QtGraphicalEffects 1.0

Rectangle {
  width: 512
  height: 512
  color: '#000000'

  property real angle: 0.0

  NumberAnimation on angle {
    duration: 4000
    from: 0.0
    to: 2.0*Math.PI
    loops: Animation.Infinite
  }

  onAngleChanged: sweep.advance(angle)

  // Render a radar-sweep like thing with a shader
  // NB we want to render all pixels touched since last draw, not just draw a line
  ShaderEffect {
    id: sweep
    anchors.fill: parent
    blending: false

    function advance(a) {a0=a1;a1=a;}

    property real a0: 0.0
    property real a1: 0.0
    readonly property real pi: Math.PI

    vertexShader: "
      uniform highp mat4 qt_Matrix;
      attribute highp vec4 qt_Vertex;
      attribute highp vec2 qt_MultiTexCoord0;
      varying highp vec2 coord;
      void main() {
        coord=qt_MultiTexCoord0;
        gl_Position=qt_Matrix*qt_Vertex;
      }"
    fragmentShader: "
      uniform highp float a0;
      uniform highp float a1;
      uniform highp float pi;
      varying highp vec2 coord;
      void main() {
        highp float a=pi+atan(coord.y-0.5,coord.x-0.5);
        // TODO: Might be better if had persist's decay rate built in and pixels at back of sector were reduced
        bool hit=(
          distance(coord,vec2(0.5,0.5))<0.48
          && (
            (a0<a && a<=a1) 
            || 
            (a1<a0 && ((0.0<=a && a<=a1) || (a0<a && a<2.0*pi)))  // Wrap-round case
          )
        );
        gl_FragColor=(hit ? vec4(0.0,1.0,0.0,1.0) : vec4(0.0,0.0,0.0,1.0));
      }"
  }

  // Capture to use radar sweep in persistence effect
  ShaderEffectSource {
    id: base
    anchors.fill: parent
    live: true
    sourceItem: sweep
    hideSource: true
  }

  // Display persistence effect using a recursive shader effect
  ShaderEffectSource {
    id: persist
    anchors.fill: parent
    //live: true
    recursive: true
    sourceItem: persist
    wrapMode: ShaderEffectSource.ClampToEdge

    ShaderEffect {
      anchors.fill: parent
      blending: false
      property variant src0: persist
      property variant src1: base
      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord=qt_MultiTexCoord0;
          gl_Position=qt_Matrix*qt_Vertex;
        }"
      fragmentShader: "
        uniform sampler2D src0;
        uniform sampler2D src1;
        varying highp vec2 coord;
        void main() {
          highp vec4 src0=texture2D(src0,coord);
          highp vec4 src1=texture2D(src1,coord);
          gl_FragColor=src1+floor(255.0*0.99*src0)/255.0;
        }"
    }
  }

  // Bit of blur to break up quantization
  // and just to make sure there's lots of GPU load
  GaussianBlur {
    id: glow
    anchors.fill: parent
    source: persist
    radius: 8
    samples: 16
  }
}
