import QtQuick 2.2

Rectangle {
  id: main
  width: 660
  height: 660
  color: '#000088'

  Timer {
    interval: 1000/60
    running: true
    repeat: true
    onTriggered: {advance();display();}
  }

  Component {
    id: cellComponent
    Rectangle {
      objectName: 'cell'
      property int row: 0
      property int col: 0      
      x: main.width/2+width*col
      y: main.height/2+height*row      
      width: 3
      height: 3
      radius: 1.5
      smooth: true
      color: '#ffcc00'
    }
  }

  property var cells: null

  Component.onCompleted: {
    cells=[[-1, 0],[-1, 1],[ 0,-1],[ 0, 0],[ 1, 0]];
    display();
  }

  function display() {
    // Just completely regenerate display field each frame
    // TODO: might be nicer to do differential updates, would allow birth/death animations   

    // Nuke all previously displayed cells
    for (var i=0;i<children.length;i++) {
      if (children[i].objectName=='cell') {
        children[i].destroy();
      }
    }

    // Show current set of cells
    for (var i=0;i<cells.length;i++) {
      var c=cellComponent.createObject(
        main,
        {'row':cells[i][0],'col':cells[i][1]}
      );
    }
  }

  function advance() {

    // Build a hash of the currently alive cells and a neighbour count (includes self)
    var a=new Object;
    var n=new Object;
    for (var i=0;i<cells.length;i++) {
      var p=cells[i]
      var r=p[0];
      var c=p[1];
      if (!(r in a)) a[r]=new Object;
      a[r][c]=1;
      for (var dr=r-1;dr<=r+1;dr++) {
        for (var dc=c-1;dc<=c+1;dc++) {
          if (!(dr in n)) n[dr]=new Object;
          if (!(dc in n[dr])) n[dr][dc]=0;
          n[dr][dc]+=1;
        }
      }
    }

    // For all live cells, assess viability
    var kill=[];
    var stay=[];
    for (var r in a) {
      for (var c in a[r]) {
        if (n[r][c]-1<2 || n[r][c]-1>3)
          kill.push([Number(r),Number(c)]);
        else
          stay.push([Number(r),Number(c)]);
      }
    }

    // For neighbours of live cells, assess potential for births
    var born=[];
    for (var r in n) {
      for (var c in n[r]) {
        if (!((r in a) && (c in a[r]))) {
          if (n[r][c]==3)
            born.push([Number(r),Number(c)]);
        }
      }
    }   

    cells=stay.concat(born)
  }
}
