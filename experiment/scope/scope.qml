import QtQuick 2.2
import QtGraphicalEffects 1.0

Item {
  id: main
  width: 640
  height: 480

  Rectangle {
    id: bg
    anchors.fill: parent
    color: '#50593a'
    visible: false
  }

  Rectangle
  {
    id: phosphor
    anchors.fill: parent
    color: '#00000000'

    property real timebase: 0.0

    NumberAnimation on timebase {
      duration: 1000
      from: 0.0
      to: 1.0
      loops: Animation.Infinite
    }

    onTimebaseChanged: trace.advance(timebase)

    Item {
      id: trace
      anchors.fill: parent

      property real cycles: 0.0
      property real noise: 0.0

      function advance(t) {
	if (t<spot.x1) cycles+=1.0
        spot.x0=spot.x1
	spot.x1=t

        var signal=0.2+0.6*Math.pow(0.5+0.5*Math.cos(2.0*Math.PI*(cycles+t)*0.8),16)
        noise+=(Math.random()-0.5)*0.01-0.01*noise

	spot.y0=spot.y1
        spot.y1=1.0-signal-noise
      }

      ShaderEffect {
        id: spot
        anchors.fill: parent
        blending: true
        
        property real x1: 0.0
        property real y1: 0.0
        property real x0: 0.0
        property real y0: 0.0
        property color col: '#7fffa7'

        vertexShader: "
          uniform highp mat4 qt_Matrix;
          attribute highp vec4 qt_Vertex;
          attribute highp vec2 qt_MultiTexCoord0;
          varying highp vec2 coord;
          void main() {
            coord=qt_MultiTexCoord0;
            gl_Position=qt_Matrix*qt_Vertex;
          }"
        fragmentShader: "
          uniform highp float x0;
          uniform highp float y0;
          uniform highp float x1;
          uniform highp float y1;
          uniform lowp vec4 col;
          varying highp vec2 coord;
          void main() {
            highp float k=1.0/distance(vec2(x1,y1),vec2(x0,y0));
            highp float r=0.0001+0.00005*k;
            highp vec2 n=normalize(vec2(x1,y1)-vec2(x0,y0));
            highp float p=dot(coord,n)-dot(vec2(x0,y0),n);
            highp float q=p*k;
            highp float d=distance(coord,vec2(x0,y0)+p*n);
            highp float d0=distance(coord,vec2(x0,y0));
            highp float d1=distance(coord,vec2(x1,y1));
            if (x1<x0)
              gl_FragColor=vec4(0.0,0.0,0.0,0.0);            
            else if (0.0<=q && q<=1.0 && d<r)
              gl_FragColor=col*(1.0-d/r);
            else if (q<0.0 && d0<r)
              gl_FragColor=col*(1.0-d/r);
            else if (q>1.0 && d1<r)
              gl_FragColor=col*(1.0-d/r);
            else
              gl_FragColor=vec4(0.0,0.0,0.0,0.0);
          }"
      }
    }

    ShaderEffectSource {
      id: base
      anchors.fill: parent
      live: true
      sourceItem: trace
      hideSource: true
    }
    
    ShaderEffectSource {
      id: buffer
      anchors.fill: parent
      live: true
      recursive: true
      sourceItem: buffer
      wrapMode: ShaderEffectSource.ClampToEdge
      smooth: false
  
      ShaderEffect {
        anchors.fill: parent
        blending: false
        property variant src0: buffer
        property variant src1: base
        vertexShader: "
          uniform highp mat4 qt_Matrix;
          attribute highp vec4 qt_Vertex;
          attribute highp vec2 qt_MultiTexCoord0;
          varying highp vec2 coord;
          void main() {
            coord=qt_MultiTexCoord0;
            gl_Position=qt_Matrix*qt_Vertex;
          }"
        fragmentShader: "
          uniform sampler2D src0;
          uniform sampler2D src1;
          varying highp vec2 coord;
          void main() {
            highp vec4 src0=texture2D(src0,coord);
            highp vec4 src1=texture2D(src1,coord);
            gl_FragColor=floor(255.0*0.975*src0)/255.0+src1;
          }"
      }
    }
  }

  Glow {
    id: glow
    anchors.fill: parent
    radius: 16
    samples: 32
    spread: 0.75
    color: '#7fffa7'
    source: phosphor
  }

  Blend {
    anchors.fill: parent
    source: bg
    foregroundSource: glow
    mode: 'addition'
  }

  Grid {
    id: graticule
    columns: 8
    rows: 6
    spacing: 0
    Repeater {
      model: parent.rows*parent.columns
      Rectangle {
        width: 80
        height: 80
        color: '#00000000'
        border.width: 1
        border.color: '#66000000'
      }
    }
  }
}
