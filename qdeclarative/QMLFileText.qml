import QtQuick 1.1

import "prettify.js" as Prettify

MonoText {
  x: viewer.width/16.0
  width: viewer.width-2.0*x
  anchors.verticalCenter: parent.verticalCenter
  property string source: undefined
  color: "#111111"
  text: ""
  clip: true

  property real nominalFontPixelSize: 24
  font.pixelSize: nominalFontPixelSize*viewer.height/600.0

  Component.onCompleted: {
    text=Prettify.qml(
      viewer.getFileContents(source)
    );
  }
  onTextChanged: {
    while (paintedHeight>parent.bodyHeight) {
      nominalFontPixelSize*=0.95;
    }
  }
}
