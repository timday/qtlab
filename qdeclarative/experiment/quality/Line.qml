import QtQuick 1.1

Rectangle {
  width: main.width
  height: 3.0
  y: 0.5*main.height
  color: '#000000'
  smooth: true
  transformOrigin: Item.Center

  Rectangle {
    width: parent.width-2
    height: 1.0
    x: 1.0
    y: 1.0
    color: '#00ff00'
    smooth: parent.smooth
  }

  Text {
    visible: (parent.rotation==0.0)
    x: 0.0
    y: parent.height
    width: 0.5*parent.width
    height: paintedHeight
    text: "Testing 123"
    font.pixelSize: 24
    font.family: "Droid Sans"
    color: '#ffff00'
    smooth: true
    style: Text.Outline
    styleColor: '#000000'
  }

}
