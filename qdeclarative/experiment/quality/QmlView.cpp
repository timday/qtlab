#include "QmlView.h"

QmlView::QmlView(const QString& src,bool hqaa,bool opengl,bool msaa) 
  :QDeclarativeView(0)
{
  setResizeMode(QDeclarativeView::SizeViewToRootObject);

  if (hqaa)
    setRenderHints(QPainter::HighQualityAntialiasing);

  if (opengl) {
    QGLFormat gl_format(QGLFormat::defaultFormat());
    gl_format.setSampleBuffers(msaa);
    QScopedPointer<QGLWidget> glWidget(new QGLWidget(gl_format));
    //glWidget->setAutoFillBackground(true);
    //glWidget->setAttribute(Qt::WA_NoSystemBackground);
    //glWidget->setAttribute(Qt::WA_OpaquePaintEvent);
    setViewport(glWidget.take());
  }

  setWindowTitle
    (
     (hqaa ? QString("HQAA ") : QString())
     +
     (opengl ? QString("OpenGL") : QString())
     +
     (msaa ? QString("&MSAA") : QString())
     );
  rootContext()->setContextProperty("viewer",this);

  load(src);

  QObject::connect(this,SIGNAL(pressedKeyQ()),QCoreApplication::instance(),SLOT(quit()));
}

QmlView::~QmlView() {}

void QmlView::keyPressEvent(QKeyEvent* event) {
if (event->key()==Qt::Key_Q) {
    emit pressedKeyQ();
    event->accept();
  }
  QDeclarativeView::keyPressEvent(event);
}

void QmlView::resizeEvent(QResizeEvent* event) {
  emit widthChanged(width());
  emit heightChanged(height());
  QDeclarativeView::resizeEvent(event);
}

void QmlView::load(const QUrl& url) {
  QMetaObject::invokeMethod(this,"loadImmediate",Qt::QueuedConnection,Q_ARG(QUrl,url));
}

void QmlView::loadImmediate(const QUrl& url) {
  std::cerr << "[Setting source " << url.toString().toLocal8Bit().data() << "]";
  setSource(url);

  const QList<QDeclarativeError> errs=errors();  
  for (int i=0;i<errs.size();i++) {
    std::cerr << "[" << errs[i].toString().toLocal8Bit().data() << "]";
  }
}
