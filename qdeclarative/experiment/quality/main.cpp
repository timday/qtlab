#include "QmlView.h"

int main(int argc,char* argv[])
{

  if (argc<2) {
    std::cerr << "Provide a qml file as argument\n";
    exit(1);
  }
  QApplication app(argc,argv);

  QmlView view0(app.arguments()[1],false,false,false);
  QmlView view1(app.arguments()[1],true, false,false);
  QmlView view2(app.arguments()[1],false,true ,false);
  QmlView view3(app.arguments()[1],true, true, false);
  QmlView view4(app.arguments()[1],false,true, true );
  QmlView view5(app.arguments()[1],true, true, true );

  view0.show();
  view1.show();
  view2.show();
  view3.show();
  view4.show();
  view5.show();
  return app.exec();
}
