CONFIG += precompile_header release 

QT += core gui declarative opengl

TARGET = quality
TEMPLATE = app

SOURCES += $$system(ls *.cpp)
HEADERS += $$system(ls *.h)
PRECOMPILED_HEADER = precompiled.h

DESTDIR = build
OBJECTS_DIR = build/.obj
MOC_DIR = build/.moc
RCC_DIR = build/.qrc
UI_DIR = build/.ui

unix:!macx{
  LIBS += -lGLU
}

macx {
#  QT += multimedia concurrent multimediawidgets # Qt 5.1
}
