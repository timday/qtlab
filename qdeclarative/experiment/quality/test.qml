import QtQuick 1.1

Rectangle {
  id: main
  width: 256
  height: 256
  color: '#888888'
  smooth: true

  Component.onCompleted: {
    var component=Qt.createComponent("Line.qml")
    for (var a=0;a<9;a++) {
      var c=component.createObject(main,{"rotation":10.0*a})
    }
  }
}
