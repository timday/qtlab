#ifndef _common_h_
#define _common_h_

#include <cmath>
#include <iostream>
#include <stdexcept>
#include <vector>

#include <QApplication>

#include <QColor>
#include <QDeclarativeContext>
#include <QDeclarativeError>
#include <QDeclarativeItem>
#include <QDeclarativeProperty>
#include <QDeclarativeView>
#include <QFile>
#include <QGLFormat>
#include <QGLWidget>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QImage>
#include <QKeyEvent>
#include <QPushButton>
#include <QRegExp>
#include <QScopedPointer>
#include <QtCore>
#include <QTextStream>
#include <QThread>
#include <QTime>
#include <QTimer>
#include <QVariant>

#endif
