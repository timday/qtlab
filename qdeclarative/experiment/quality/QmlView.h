#ifndef _qmlview_h_
#define _qmlview_h_

class QmlView : public QDeclarativeView {
  Q_OBJECT
  Q_PROPERTY(int width READ width NOTIFY widthChanged)
  Q_PROPERTY(int height READ height NOTIFY heightChanged)

public:

  QmlView(const QString&,bool hqaa,bool opengl,bool msaa);
  ~QmlView();

  void keyPressEvent(QKeyEvent*);
  void resizeEvent(QResizeEvent*);

public slots:

  void load(const QUrl&);

signals:

  void widthChanged(int);
  void heightChanged(int);
  void pressedKeyQ();

private slots:

  void loadImmediate(const QUrl&);

};

#endif
