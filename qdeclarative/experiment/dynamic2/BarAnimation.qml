import QtQuick 1.1

PropertyAnimation {
  target: bar
  running: true
  loops: Animation.Infinite
  property: "scale"
  from: 0.0
  to: 1.0
}
