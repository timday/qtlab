import QtQuick 1.1

Rectangle {
  width: 500
  height: 500
  color: '#0000ff'

  // Any way to identify index in parent?
  Item {
    Component.onCompleted: {
      for (var i=0;i<parent.children.length;i++) if (parent.children[i]==parent) console.log(i)
    }
  }
}
