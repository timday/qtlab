import QtQuick 1.1

Rectangle {
  id: main
  width: 100
  height: 100
  color: '#ffffff'

  Component.onCompleted: {
    var foo=Qt.createComponent("Foo.qml")
    var a=foo.createObject(main)

    var bar=Qt.createComponent("Bar.qml")
    var b=bar.createObject(a)
  }
}
