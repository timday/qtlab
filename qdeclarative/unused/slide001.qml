import QtQuick 1.1

Slide {
  titletext: "In 5 minutes..."

  Bullets {
    Bullet {delay:0;text: "What's the problem I'm trying to solve?"}
    Bullet {delay:1;text: "Qt & QtQuick/QML - what are they?"}
    SubBullet {delay:2;text: "A few QML code samples"}
    Bullet {delay:3;text: "2 core QtQuick features:"}
    SubBullet {delay:4;text: "Property binding"}
    SubBullet {delay:5;text: "C++ component integration"}
  }
}
