import QtQuick 1.1

Slide {
  titletext: 'Qt versions'

  Bullets {
    Bullet {delay:0;text: "<b>Qt3</b> series: conventional C++ GUI widgets library"}
    SubBullet {delay:1;text: 'Unique features: signals and slots system, "Meta-Object Compiler"'}
    Bullet {delay:2;text: "<b>Qt4</b> series: improvements to layout, graphics, multimedia, web and mobile influences..."}
    Bullet {delay:3;text: "<b>Qt4.7</b>: QtQuick and QML appear"}
    SubBullet {delay:4;text: "Strong mobile focus: Symbian and Maemo!"}
    Bullet {delay:5;text: "<b>Qt5</b>: Major overhaul.  QtQuick/QML performance, QtQuick for desktop catches up"}
    Bullet {delay:6;text: '<b>Qt5.2</b> (in beta): "Qt Everywhere" support for Android, iOS and Blackberry 10'}
  }
}
