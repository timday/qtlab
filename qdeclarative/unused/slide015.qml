import QtQuick 1.1

Slide {
  next: "slide016.qml"
  prev: "slide014.qml"
  titletext: "Running QML"

  CGoLPlayer {
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
  }
}
