import QtQuick 1.1

Item {
  width: cgol.width
  height: cgol.height+ctrl.height

  CGoL {
    id: cgol
    running: false
    animate: false
  }

  Row {
    id: ctrl
    anchors.top: cgol.bottom
    anchors.horizontalCenter: cgol.horizontalCenter
    width: parent.width
    height: 32

    Image {
      source: "icon/Gnome-edit-delete.svg"
      height: parent.height
      width: ctrl.width/ctrl.children.length
      smooth: true
      fillMode: Image.PreserveAspectFit
      MouseArea {
        anchors.fill: parent
        onClicked: {cgol.clear();cgol.running=false;}
      }
    }

    Image {
      source: (cgol.running ? "icon/Gnome-media-playback-pause.svg"  : "icon/Gnome-media-playback-start.svg")
      height: parent.height
      width: ctrl.width/ctrl.children.length
      smooth: true
      fillMode: Image.PreserveAspectFit
      MouseArea {
        anchors.fill: parent
        onClicked: cgol.running = !cgol.running
      }
    }

    Text {
      id: rpent
      horizontalAlignment: Text.AlignHCenter 
      verticalAlignment: Text.AlignVCenter
      text: "Setup\nR-Pentomino"
      smooth: true
      height: parent.height
      width: ctrl.width/ctrl.children.length
      states: [
        State {name: ""; PropertyChanges {target: rpent;font.weight: Font.Normal}},
        State {name: "HOVER"; PropertyChanges {target: rpent;font.weight: Font.Bold}}
      ]
      MouseArea {
        hoverEnabled: true
        anchors.fill: parent
        onClicked: cgol.initRPentomino();
        onEntered: parent.state="HOVER"
        onExited: parent.state=""
      }
    }

    Row {
      id: anim
      height: parent.height
      width: ctrl.width/ctrl.children.length
      states: [
        State {name: ""; PropertyChanges {target: animtxt;font.weight: Font.Normal}},
        State {name: "HOVER"; PropertyChanges {target: animtxt;font.weight: Font.Bold}}
      ]
      Image {
        source: (cgol.animate ? "icon/yes.svg" : "icon/no.svg")
        height: parent.height
        width: height
        smooth: true
        fillMode: Image.PreserveAspectFit    
        MouseArea {
          hoverEnabled: true
          anchors.fill: parent
          onClicked: cgol.animate = !cgol.animate;
          onEntered: anim.state="HOVER"
          onExited: anim.state=""
        }
      }
      Text {
        id: animtxt
        height: parent.height
        width: paintedWidth
        verticalAlignment: Text.AlignVCenter
        text: "Animate"
        smooth: true      
        MouseArea {
          hoverEnabled: true
          anchors.fill: parent
          onClicked: cgol.animate = !cgol.animate;
          onEntered: anim.state="HOVER"
          onExited: anim.state=""
        }
      }
    }
  }
}
