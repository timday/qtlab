#include "QmlViewer.h"
#include "Mbrot.h"

int main(int argc,char* argv[])
{
  QApplication app(argc,argv);

  qmlRegisterType<Mbrot>("Mbrot",1,0,"Mbrot");
 
  QmlViewer viewer(app.arguments().mid(1));

  viewer.show();
  return app.exec();
}
