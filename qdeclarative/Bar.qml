import QtQuick 1.1

Rectangle {
  id: bar
  y: parent.height-height
  width: parent.width/(1.0+parent.children.length)
  height: parent.height*value/250.0
  color: '#00ff00'
  opacity: 0.9

  property int value: 0

  Behavior on height {
    ParallelAnimation {
      PropertyAnimation {}
      PropertyAnimation {
        target: bar
        property: "color"
        from: '#ff0000'
        to: '#00ff00'
      }
    }
  }

  SansText {
    anchors.bottom: parent.bottom
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 24.0*viewer.height/600.0
    text: parent.value.toString()
  }
}
