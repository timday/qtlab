import QtQuick 1.1

SansText {
  property alias subtext: sub.text
  width: paintedWidth
  height: paintedHeight
  anchors.horizontalCenter: parent.horizontalCenter
  anchors.verticalCenter: parent.verticalCenter

  text: ""
  font.pixelSize: 64*viewer.height/600.0

  SansText {
    id: sub
    width: paintedWidth
    height: paintedHeight
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.bottom
    font.pixelSize: 0.5*parent.font.pixelSize
  }
}
