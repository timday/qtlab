import QtQuick 1.1

ParallelAnimation {

  property real dstx: -0.5*slide.width                      +slide.width     *(slide.galleryIndex%6+0.5)/6.0
  property real dsty: -0.5*(slide.height-slide.headerHeight)+slide.bodyHeight*(0.33*(0.5+Math.floor(slide.galleryIndex/6)))

  PropertyAnimation {
    target: slide
    property: "x"
    from: -0.5*slide.width
    to: dstx
    duration: 1000
    easing.type: Easing.OutQuad
  }
  PropertyAnimation {
    target: slide
    property: "y"
    from: -0.5*slide.height
    to: dsty
    duration: 1000
    easing.type: Easing.OutBounce
  }
  PropertyAnimation {
    target: slide
    property: "rotation"
    from: -360.0*Math.random()
    to: -30.0+60.0*Math.random()
    duration: 1000
    easing.type: Easing.OutQuad
  }
}
