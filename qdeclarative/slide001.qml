import QtQuick 1.1

Slide {
  titletext: "The innovator's headache..."

  Bullets {
    Bullet {delay:0;text: 'You have a new C++ "compute engine"'}
    SubBullet {delay:2;text: "You have a commandline app / testsuite.  Easy."}
    Bullet {delay:4;text: "You want an interactive demo app.  Problems:"}
    SubBullet {delay:6;text: 'C++ GUI programming is fiddly and tedious'}
    SubBullet {delay:8;text: 'Unsatisfactory "plugin" architectures'}
    Bullet {delay:10;text: "<b>What if</b> there was a framework designed around smoothly mixing C++ components into a modern dynamic web-influenced system for UI creation?"}    
  }
}
