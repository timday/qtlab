#ifndef _common_h_
#define _common_h_

#include <iostream>
#include <vector>
#include <cmath>

#include <QApplication>

#include <QColor>
#include <QDeclarativeContext>
#include <QDeclarativeError>
#include <QDeclarativeItem>
#include <QDeclarativeProperty>
#include <QDeclarativeView>
#include <QFile>
#include <QGraphicsView>
#include <QGLFormat>
#include <QGLWidget>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QImage>
#include <QKeyEvent>
#include <QPushButton>
#include <QPrinter>
#include <QRegExp>
#include <QScopedPointer>
#include <QtCore>
#include <QTextStream>
#include <QThread>
#include <QTime>
#include <QTimer>
#include <QVariant>

#include <QtConcurrentMap>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
#include <QtConcurrent>
#endif

#endif
