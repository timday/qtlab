#ifndef _mbrot_h_
#define _mbrot_h_

class Mbrot : public QDeclarativeItem {
  Q_OBJECT
  Q_PROPERTY(QPointF centre READ centre WRITE setCentre)
  Q_PROPERTY(qreal radius READ radius WRITE setRadius)
  Q_PROPERTY(QList<QVariant> colors READ colors WRITE setColors)
  Q_PROPERTY(bool downsize READ downsize WRITE setDownsize)

public:
  Mbrot();
  ~Mbrot();

  QPointF centre() const {return _centre;}
  void setCentre(const QPointF& p) {_centre=p;recalculate();}
  qreal radius() const {return _radius;}
  void setRadius(qreal r) {_radius=r;recalculate();}
  QList<QVariant> colors() const {return _colors;}
  void setColors(const QList<QVariant>& c) {_colors=c;recalculate();}
  bool downsize() const {return _downsize;}
  void setDownsize(bool d) {_downsize=d;if (!_downsize) recalculate();}

  Q_INVOKABLE void moveCentre(const QPointF&,const QPointF&);
  Q_INVOKABLE QPointF positionOfPixel(int,int) const;

  void paint(QPainter* painter,const QStyleOptionGraphicsItem*,QWidget*);
  
protected:

  void geometryChanged(const QRectF&,const QRectF&);

private:
  QImage _image;
  QPointF _centre;
  qreal _radius;
  bool _downsize;
  QList<QVariant> _colors;

  void recalculate();
};

#endif
