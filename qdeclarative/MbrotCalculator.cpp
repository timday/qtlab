#include "MbrotCalculator.h"

namespace {

  template <typename T> struct Invoker {

    Invoker(T& tgt)
    :_tgt(tgt)
    {}
    
    template <typename U> void operator()(U x) const {
      return _tgt(x);
    }
    
    T& _tgt;
  };

  static int iterations(const double cx,const double cy,const int n) {
    double x=cx;
    double y=cy;
    double x2=x*x;
    double y2=y*y;
    int i=0;
    while (i<n && x2+y2<=4.0) {
      i++;
      y=2.0*x*y+cy;
      x=x2-y2+cx;
      x2=x*x;
      y2=y*y;
    }
    return i;
  }
 
}

MbrotCalculator::MbrotCalculator(int width,int height,double ox,double oy,double r,int n,const QList<QVariant>& colors)
  :_width(width)
  ,_height(height)
  ,_ox(ox)
  ,_oy(oy)
  ,_s(r/(0.5*std::min(width,height)))
  ,_n(n)
{
  _image=QImage(_width,_height,QImage::Format_ARGB32);

  for (int i=0;i<colors.length();i++) {
    _colors.push_back(colors[i].value<QColor>().rgba());
  }
  while (_colors.length()<6) {
    _colors.push_back(QColor(128,128,128,128).rgba());
  }
  
  std::vector<std::pair<int,QRgb*> > lines(height);
  for (int y=0;y<height;++y) {
    lines[y]=std::make_pair(y,reinterpret_cast<QRgb*>(_image.scanLine(y)));
  }
  
  Invoker<MbrotCalculator> inv(*this);
  QtConcurrent::blockingMap(lines.begin(),lines.end(),inv);
}

MbrotCalculator::~MbrotCalculator()
{}

QRgb blend(const QRgb& a,const QRgb& b,float p) {
  const unsigned int a0=(a&0xff);
  const unsigned int b0=(b&0xff);
  const unsigned int c0=(static_cast<int>(a0+p*(b0-a0)) & 0xff);

  const unsigned int a1=((a&0xff00)>>8);
  const unsigned int b1=((b&0xff00)>>8);
  const unsigned int c1=(static_cast<int>(a1+p*(b1-a1)) & 0xff);

  const unsigned int a2=((a&0xff0000)>>16);
  const unsigned int b2=((b&0xff0000)>>16);
  const unsigned int c2=(static_cast<int>(a2+p*(b2-a2)) & 0xff);

  const unsigned int a3=((a&0xff000000)>>24);
  const unsigned int b3=((b&0xff000000)>>24);
  const unsigned int c3=(static_cast<int>(a3+p*(b3-a3)) & 0xff);

  return ((c3<<24)|(c2<<16)|(c1<<8)|c0);
}

void MbrotCalculator::operator()(const std::pair<int,QRgb*>& line) const {
  const int y=line.first;
  QRgb*const pixels=line.second;
  
  for (int x=0;x<_width;++x) {
    const int i=iterations(_ox+_s*(x-_width*0.5),_oy+_s*(y-_height*0.5),_n);
    if (i==0) {
      pixels[x]=_colors[0];
    } else if (i==_n) {
      pixels[x]=_colors[1];
    } else if (i%2==0) {
      pixels[x]=blend(_colors[2],_colors[3],(i%256+0.5f)/256.0f);
    } else {
      pixels[x]=blend(_colors[4],_colors[5],(i%256+0.5f)/256.0f);
    }
  }
}

//QColor(0,0,128,255) // Inner
//QColor(0,0,255,i%256) // Ramp
