import QtQuick 1.1

Item {
  id: button
  property variant colors: undefined;

  anchors.horizontalCenter: parent.horizontalCenter
  width: 0.75*parent.width
  smooth: true
  
  Rectangle {
    anchors.fill: bars
    smooth: true
    color: '#ffffff'
  }

  Row {
    id: bars

    spacing: 0
    height: parent.height
    anchors.verticalCenter: parent.verticalCenter

    Rectangle {
      width: 0.25*button.width
      height: parent.height
      smooth: true
      color: colors[0]
    }
    Rectangle {
      width: 0.25*button.width
      height: parent.height
      smooth: true
      gradient: Gradient {
        GradientStop {position: 0.1;color: button.colors[2];}
        GradientStop {position: 0.9;color: button.colors[3];}
      }
    }
    Rectangle {
      width: 0.25*button.width
      height: parent.height
      smooth: true
      gradient: Gradient {
        GradientStop {position: 0.1;color: button.colors[4];}
        GradientStop {position: 0.9;color: button.colors[5];}
      }
    }
    Rectangle {
      width: 0.25*button.width
      height: parent.height
      smooth: true
      color: colors[1]
    }
  }

  Rectangle {
    id: highlight
    visible: false
    anchors.fill: bars
    color: '#00000000'
    smooth: true
    opacity: 1.0
    border.width: 4
    border.color: '#ff0000'
  }

  MouseArea {
    anchors.fill: parent
    hoverEnabled: true
    onEntered: highlight.visible=true
    onExited: highlight.visible=false
    onClicked: mbrot.colors=colors;
  }
}
