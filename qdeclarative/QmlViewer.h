#ifndef _qmlviewer_h_
#define _qmlviewer_h_

class QmlViewer : public QDeclarativeView {
  Q_OBJECT
  Q_PROPERTY(int width READ width NOTIFY widthChanged)
  Q_PROPERTY(int height READ height NOTIFY heightChanged)
  Q_PROPERTY(int slide READ slide NOTIFY slideChanged)
  Q_PROPERTY(int slides READ slides CONSTANT)
  Q_PROPERTY(qreal seconds READ seconds NOTIFY secondsChanged)

public:

  QmlViewer(const QStringList&);
  ~QmlViewer();

  Q_INVOKABLE QString getFileContents(const QString&);

  int slide() {return _slide;}
  int slides() {return _slides.length();}
  qreal seconds() {return _seconds;}

  void keyPressEvent(QKeyEvent*);
  void resizeEvent(QResizeEvent*);

public slots:

  void toggleTimer();
  void move(int);
  void load(const QUrl&);
  void print();

signals:

  void widthChanged(int);
  void heightChanged(int);
  void slideChanged(int);
  void secondsChanged(int);
  void contentChanged(const QString&);
  void pressedKeyEsc();
  void pressedKeySpace();
  void pressedKeyF();
  void pressedKeyP();
  void pressedKeyQ();

private slots:

  void loadImmediate(const QUrl&);
  void tick();

private:
  int _slide;
  QStringList _slides;
  qreal _seconds;
  QTimer _clock;
};

#endif
