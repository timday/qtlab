General:

- Latex integration; see https://github.com/dragly/latex-presentation ; would want to bring in PDF elements rather than images probably though.

Slides for:

* New title: A Quick Look at QtQuick

* De-emphasisze desktop UI.  Emphasisize quickly exposing interesting C++ functionality behind a minimal interface.

* A C++ component.  Show some more UI around the C++ component.  Pass in 6 colours: in, out and 2 gradients?

* Show some of the viewer (ctor only MyView.cpp) for completeness.

* Futures:

    * "Qt Quick Controls" (formerly "Qt Desktop Components": QtQuick components relevant to desktop developers; e.g menus, layouts)

    * QmlWeb: <http://akreuzkamp.de/2013/07/10/webapps-written-in-qml-not-far-from-reality-anymore/> - webapps for "QML to DOM and CSS".
      Intro makes some comments about ease-of-use of QML.

    * "Ubuntu phone" was actually a "QML phone".  See also http://developer.ubuntu.com/apps/qml/

* "All slides" collection at end.

Mention:

* Documentation is fantastic.

* Whole areas of QML not touched on e.g States and transitions.

* QtConcurrent: some nice map/reduce primitives (probably work well with lambdas, std::function etc)

* Mobile focus: QML got gesture support before it got mouse-wheel support.

Extras:

* Two column file display.

* Use http://google-code-prettify.googlecode.com/svn/trunk/README.html as code prettyprinter somehow.

* Refactor danger bars.

* Ball on tip of timer danger bars.  Or particles.

* Downsized/deferred full-res Mbrot rendering with QFuture/QFutureWatcher?

* A C++ OpenGL component (no, too much)

Not possible:

* Do something with QtQuick.Dialogs' ColorDialog, but think it needs QT5's nextgen declarative view.
