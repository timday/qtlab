import QtQuick 1.1

Rectangle {
  id: footer
  anchors.bottom: slide.bottom
  width: slide.width
  height: slide.footerHeight
  color: '#4444ff'
  smooth: true

  Image {
    id: qtlogo
    source: "icon/qtlogo.svg"
    anchors.right: footer.right
    anchors.rightMargin: 0.1*footer.height
    anchors.verticalCenter: parent.verticalCenter
    height: 0.8*footer.height
    width: (height*sourceSize.width)/sourceSize.height
    smooth: true
    fillMode: Image.PreserveAspectFit
  }

  property int slidesLeft: viewer.slides-viewer.slide
  property real timeLeft: Math.max(0.0,300.0-viewer.seconds)
  property real timePerSlide: timeLeft/slidesLeft
  property real slideStartTime: viewer.seconds

  Component.onCompleted: slideStartTime=viewer.seconds

  Rectangle {
    visible: (slide.state=="NORMAL")
    y: 0.6*parent.height
    width: parent.width*(viewer.seconds-slideStartTime)/timePerSlide
    height: 0.1*parent.height
    property real danger: width/parent.width
    color: Qt.rgba(Math.sqrt(danger),Math.sqrt(1.0-danger),0.0,1.0)
    Behavior on width {PropertyAnimation {duration: 100}}
  }

  Rectangle {
    visible: (slide.state=="NORMAL")
    y: 0.8*parent.height
    width: parent.width*viewer.seconds/300.0
    height: 0.1*parent.height
    property real danger: width/parent.width
    color: Qt.rgba(Math.sqrt(danger),Math.sqrt(1.0-danger),0.0,1.0)
    Behavior on width {PropertyAnimation {duration: 100}}
  }  
}
