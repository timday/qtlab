import QtQuick 1.1

Slide {
  titletext: '<b>Qt</b> ("cute" or "cutie" or "Q.T")'

  Bullets {
    Bullet {delay:0;text: "Qt: multiplatform C++ application development framework"}
    SubBullet {delay:2;text: "Started in 1995; basis for KDE from 1996"}
    SubBullet {delay:4;text: 'LGPL/GPL from Qt2.2 in 2000'}
    SubBullet {delay:6;text: "Similar to wxWidgets or gtkmm, until..."}
    Bullet {delay:8;text: "<b>QtQuick/QML</b> appears in <b>Qt4.7</b> in 2010"}
    SubBullet {delay:10;text: "Initially a strong mobile focus: Symbian and Maemo!"}
    Bullet {delay:12;text: '<b>Qt5.2</b> (now in beta): "Qt Everywhere" support for Android, iOS and Blackberry'}
  }
}
