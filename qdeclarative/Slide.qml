import QtQuick 1.1

Rectangle {
  id: slide
  width: viewer.width
  height: viewer.height
  color: '#ffffff'
  smooth: true
  focus: true
  state: "NORMAL"

  property real headerHeight: height*0.1
  property real footerHeight: headerHeight
  property real bodyHeight: height-headerHeight-footerHeight
  property int galleryIndex: -1
  property string titletext: ""

  SlideHeader {
    titletext: parent.titletext
  }
  SlideFooter {}
  Bubbles {
    visible: (parent.state=="NORMAL")
  }

  SlideShadow {
    visible: (parent.state=="GALLERY")
    z: -1
  }
  SlideGalleryAnimation {
    running: (state=="GALLERY")  // NB no parent qualifier, animations live in parent scope
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.LeftButton | Qt.RightButton
    onClicked: {
      viewer.move(mouse.button==Qt.LeftButton ? 1 : -1);
    }
  }
  Keys.onPressed: {
    if (event.key==Qt.Key_Right) {viewer.move(1);event.accepted=true;}
    else if (event.key==Qt.Key_Left) {viewer.move(-1);event.accepted=true;}
  }
}
