#ifndef _mbrotcalculator_h_
#define _mbrotcalculator_h_

class MbrotCalculator {

public:

  MbrotCalculator(int width,int height,double ox,double oy,double r,int n,const QList<QVariant>& colors);
  ~MbrotCalculator();

  QImage image() const {
    return _image;
  }
  
  void operator()(const std::pair<int,QRgb*>& line) const;
  
private:

  const int _width;
  const int _height;
  const double _ox;
  const double _oy;
  const double _s;
  const int _n;
  QList<QRgb> _colors;
  QImage _image;
};

#endif
