import QtQuick 1.1

Rectangle {
  id: controls
  anchors.left: parent.left
  anchors.verticalCenter: parent.verticalCenter
  width: 0.125*parent.width
  height: 0.75*parent.height
  radius: 0.125*width
  color: '#444444'
  opacity: 0.0025
  smooth: true

  Behavior on opacity {PropertyAnimation {}}

  MouseArea {
    anchors.fill: parent
    hoverEnabled: true
    onEntered: parent.opacity=1.0
    onExited: parent.opacity=0.0025
    acceptedButtons: Qt.NoButton  // Else will swallow stuff intended for buttons

    Column {
      id: buttons
      width: parent.width
      anchors.verticalCenter: parent.verticalCenter
      spacing: 10
      MbrotColorButton {
        height: controls.height/(parent.children.length+1)
        colors: [Qt.rgba(0,0,0,0),Qt.rgba(0,0,0.5,1),Qt.rgba(0,0,1,0),Qt.rgba(0,0,1,1),Qt.rgba(0,0,1,0),Qt.rgba(0,0,1,1)] // Tasteful blue
      }
      MbrotColorButton {
        height: controls.height/(parent.children.length+1)
        colors: [Qt.rgba(0,0,0,1),Qt.rgba(0,0,0,1),Qt.rgba(0,0,0,1),Qt.rgba(0,1,0,1),Qt.rgba(0,0,0,1),Qt.rgba(0,1,0,1)] // Oldschool green
      }
      MbrotColorButton {
        height: controls.height/(parent.children.length+1)
        colors: [Qt.rgba(0,0,0,0),Qt.rgba(0,0,0,0),Qt.rgba(1,0,0,0),Qt.rgba(1,0,0,1),Qt.rgba(1,0,0,0),Qt.rgba(1,1,0,1)] // Yellow-red
      }
      MbrotColorButton {
        height: controls.height/(parent.children.length+1)
        colors: [Qt.rgba(0,0,0,1),Qt.rgba(0,0,0,1),Qt.rgba(1,1,0,1),Qt.rgba(0,1,0,1),Qt.rgba(0,0,0,1),Qt.rgba(0,0,0,1)] // Stripy yellow
      }
      MbrotColorButton {
        height: controls.height/(parent.children.length+1)
        colors: [Qt.rgba(0,0,0,1),Qt.rgba(0,0,0,0),Qt.rgba(0,0,0,1),Qt.rgba(0,1,0,1),Qt.rgba(0,1,0,1),Qt.rgba(0,1,0,1)] // Stripy green
      }
      MbrotColorButton {
        height: controls.height/(parent.children.length+1)
        colors: [Qt.rgba(0,0,0,0),Qt.rgba(0,0,0,1),Qt.rgba(1,1,0,1),Qt.rgba(0,1,0,1),Qt.rgba(1,0,0,1),Qt.rgba(0,1,0,1)] // Convergent stripy
      }
    }
  }
}
