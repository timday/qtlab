import QtQuick 1.1

Slide {
  titletext: 'Full disclosure: QmlViewer'
  
  CPPFileText {
    id: header
    source: "QmlViewer.h"
    columns: 2
    lines: 40
  }

  Rectangle {
    color: '#80888888'
    anchors.left: header.right
    anchors.verticalCenter: parent.verticalCenter
    height: header.height
    width: 1
  }

  CPPFileText {
    source: "QmlViewer.cpp"
    anchors.left: header.right
    columns: 2
    lines: 40
  }
}
