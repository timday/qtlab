#include "Mbrot.h"
#include "MbrotCalculator.h"

Mbrot::Mbrot()
  :_centre(-1.0,0.0)
  ,_radius(1.0)
  ,_downsize(false)
{
  setFlag(QGraphicsItem::ItemHasNoContents,false);
}

Mbrot::~Mbrot() {}

void Mbrot::moveCentre(const QPointF& a,const QPointF& b) {
  _centre+=(b-a);
  recalculate();
}

QPointF Mbrot::positionOfPixel(int x,int y) const {
  const double s=_radius/(0.5*std::min(width(),height()));
  return QPointF(
    _centre.x()+s*(x-0.5*width()),
    _centre.y()+s*(y-0.5*height())
  );
}

void Mbrot::paint(QPainter* painter,const QStyleOptionGraphicsItem*,QWidget*) {
  painter->drawImage(QPointF(0.0f,0.0f),_image);
}

void Mbrot::geometryChanged (const QRectF&,const QRectF&) {
  recalculate();
  emit widthChanged();
  emit heightChanged();
}

void Mbrot::recalculate() {
  MbrotCalculator mbrot(
    width()/(_downsize ? 2 : 1),height()/(_downsize ? 2 : 1),
    _centre.x(),_centre.y(),_radius,256,_colors
  );
  _image=(_downsize ? mbrot.image().scaled(width(),height()) : mbrot.image());
  update();
}
