import QtQuick 1.1

Rectangle {
  id: bubble
  width: radius*2.0
  height: radius*2.0
  color: '#0000ff'
  opacity: 0.125
  y: parent.height+2.0*radius

  SequentialAnimation {
    running: true
    PropertyAnimation {
      target: bubble
      properties: "y"
      to: -2.0*radius
      duration: 5000
    }
    ScriptAction {
      script: bubble.destroy(0);
    }
  }
}
