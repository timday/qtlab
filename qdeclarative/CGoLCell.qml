import QtQuick 1.1

Rectangle {
  id: cell
  property int row: null
  property int column: null
  property bool animate: false
  width: 8
  height: 8
  state: "DEAD"
  states: [
    State {name: "FRESH"; PropertyChanges {target: cell;opacity: 1.0;color: '#00ff00'}},
    State {name: "ALIVE"; PropertyChanges {target: cell;opacity: 1.0;color: '#ff0000'}},
    State {name: "DEAD"; PropertyChanges {target: cell;opacity: 0.01;color: '#000000'}}  // Annoying opacity 0.0 behaviour pre-Qt5.0
  ]
  transitions: [
    Transition {
      from: (animate ? "*" : "DUMMY")
      to: "DEAD"
      PropertyAnimation {target: cell;properties:"opacity";duration: 250;easing.type: Easing.OutQuad}
    }
  ]
}
