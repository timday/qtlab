import QtQuick 1.1

Slide {
  titletext: "Property binding"
  Bullets {
    Bullet {delay:0;text: "Object properties can be assigned static values, <b>or bound to a JavaScript expression</b>"}
    SubBullet {delay:2;text: "property's value is <b>automatically updated</b> by the QML engine whenever the value of the evaluated expression changes"}
    Bullet {delay:4;text: 'Massive reduction in amount of "plumbing"/"wiring" needed'}
    SubBullet {delay:6;text: 'signal/slot wiring, or onPropertyChanged: handlers'}
    Bullet {delay:8;text: "QML engine detects dependency loops"}
    Bullet {delay:10;text: "Properties can be re-bound dynamically"}
  }
}
