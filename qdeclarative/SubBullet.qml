import QtQuick 1.1

Text {
  id: bullet
  property real delay: 0.0
  x: font.pixelSize
  height: paintedHeight
  width: parent.width-x
  text: "<default text>"
  font.pixelSize: 24*viewer.height/600.0
  wrapMode: Text.Wrap
  opacity: 0.0025

  SequentialAnimation {
    running: true
    PauseAnimation {
      duration: 1000*delay
    }
    PropertyAnimation {
      target: bullet
      property: "opacity"
      to: 1.0
    }
  }

  SubBulletIcon {}
}
