function qml(s) {
    var lines=s.split("\n");
    var regexpElement=new RegExp("^(&nbsp;)*[A-Z][A-Z|a-z]*&nbsp;")
    var regexpComment=new RegExp("//.*")
    var regexpProperty=new RegExp("^(&nbsp;)*[A-Z|a-z][A-Z|a-z|(&nbsp;)|.]*:&nbsp;")
    for (var i=0;i<lines.length;i++) {
        var line=lines[i].replace(/\t/g,"        ").replace(/\s/g,"&nbsp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
        if (regexpElement.test(line)) {
            line='<font color="#008800">'+regexpElement.exec(line)[0]+'</font>'+line.replace(regexpElement,'');
	} else if (regexpProperty.test(line)) {
	    line='<font color="#0000cc">'+regexpProperty.exec(line)[0]+'</font>'+line.replace(regexpProperty,'');
	}
        if (regexpComment.test(line)) {
            line=line.replace(regexpComment,'')+'<font color="#880000">'+regexpComment.exec(line)+'</font>';
        }
        lines[i]=line
    }
    return '<html><body style="body {white-space: pre;}">'+lines.join("<br>")+'</body></html>';  // Not sure the CSS does anything
}
