import QtQuick 1.1

Rectangle {
  id: header
  anchors.top: slide.top
  width: slide.width
  height: slide.headerHeight
  color: '#4444ff'
  smooth: true
  property string titletext: "<default text>"

  SansText {
    x: parent.height/2.0
    anchors.verticalCenter: parent.verticalCenter
    text: parent.titletext
    font.pixelSize: 48*viewer.height/600.0
    color: '#ffffff'
  }
}
