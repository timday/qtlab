import QtQuick 1.1

Item {
  property bool running: true

  property alias animate : grid.animate

  width: grid.width
  height: grid.height

  function clear() {grid.clear();}
  function initRPentomino() {grid.initRPentomino();}

  Timer {
    interval: 250
    running: parent.running
    repeat: true
    onTriggered: {grid.advance();}
  }

  Grid {
    id: grid
    property bool animate: false
  
    columns: 20
    rows: 20
    spacing: 1
  
    Repeater {
      model: parent.rows*parent.columns
      CGoLCell {row: Math.floor(index/parent.columns);column: index%parent.columns;animate: parent.animate}
    }
    
    Component.onCompleted: {initRPentomino();}
  
    property variant aliveCells: []  // Maintain list of alive state between iterations
  
    function clear() {
      setCellState(aliveCells,'DEAD');
      aliveCells=[];
    }
  
    function initRPentomino() {
      clear();
  
      var cells=[
        [-1, 0],
        [-1, 1],
        [ 0,-1],
        [ 0, 0],
        [ 1, 0]
      ]
      setCellState(cells,'FRESH');
      aliveCells=cells;
    }

    function find(p) {
      var a=aliveCells
      for (var i=0;i<a.length;i++) {
        if (a[i][0]==p[0] && a[i][1]==p[1]) return i;
      }
      return -1;
    }
    
    function setCell(row,col) {
      var r=row-rows/2
      var c=col-columns/2
      var p=[r,c]
      var i=find(p)
      var a=aliveCells;
      if (i==-1) a.push(p)
      aliveCells=a;
      children[row*columns+col].state="FRESH";
    }

    function clearCell(row,col) {
      var r=row-rows/2
      var c=col-columns/2
      var p=[r,c]
      var a=aliveCells;
      var i=find(p)
      if (i!=-1) a.splice(i,1)
      aliveCells=a;
      children[row*columns+col].state="DEAD";
    }
  
    function setCellState(cells,s) {
      for (var i=0;i<cells.length;i++) {
        var p=cells[i];
        var r=rows/2+p[0];
        var c=columns/2+p[1];
        if (0<=r && r<rows && 0<=c && c<columns) {
          children[columns*r+c].state=s;
        }
      }
    }
  
    function advance() {
  
      // Build a hash of currently alive cells and a neighbour count (includes self)
      var a=new Object;
      var n=new Object;
      for (var i=0;i<aliveCells.length;i++) {
        var p=aliveCells[i]
        var r=p[0];
        var c=p[1];
        if (!(r in a)) a[r]=new Object;
        a[r][c]=1;
        for (var dr=r-1;dr<=r+1;dr++) {
          for (var dc=c-1;dc<=c+1;dc++) {
            if (!(dr in n)) n[dr]=new Object;
            if (!(dc in n[dr])) n[dr][dc]=0;
            n[dr][dc]+=1;
          }
        }
      }
  
      // For all alive cells, assess viability
      var kill=[];
      var stay=[];
      for (var r in a) {
        for (var c in a[r]) {
          if (n[r][c]-1<2 || n[r][c]-1>3)
            kill.push([Number(r),Number(c)]);
          else
            stay.push([Number(r),Number(c)]);
        }
      }
  
      // For neighbours of live cells, assess potential
      var born=[];
      for (var r in n) {
        for (var c in n[r]) {
          if (!((r in a) && (c in a[r]))) {
            if (n[r][c]==3)
              born.push([Number(r),Number(c)]);
          }
        }
      }   
  
      setCellState(born,'FRESH');
      setCellState(stay,'ALIVE');
      setCellState(kill,'DEAD');
      
      aliveCells=stay.concat(born);
    }
  }

  MouseArea {
    anchors.fill: grid
    onClicked: {
      var it=grid.childAt(mouse.x,mouse.y);
      if (it!=null) {
        if (it.state=="DEAD") 
          grid.setCell(it.row,it.column);
        else 
          grid.clearCell(it.row,it.column);
      }
    }
  }
}
