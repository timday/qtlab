import QtQuick 1.1

import "prettify.js" as Prettify

MonoText {
  property int columns: 1
  id: txt
  anchors.left: parent.left
  anchors.leftMargin: viewer.width/16.0
  width: viewer.width/columns-anchors.leftMargin
  anchors.verticalCenter: parent.verticalCenter
  property string source: undefined
  color: "#111111"
  text: ""
  clip: true

  property int lines: -1

  property real nominalFontPixelSize: 24
  font.pixelSize: nominalFontPixelSize*viewer.height/600.0

  Component.onCompleted: {
    var t=viewer.getFileContents(source);
    text=(lines>=0 ? t.split("\n").slice(0,lines).join("\n") : t)
  }
  onTextChanged: {
    while (paintedHeight>parent.bodyHeight) {
      nominalFontPixelSize*=0.95;
    }
  }

  Rectangle {
    anchors.fill: parent
    visible: true
    gradient: Gradient {
      GradientStop {position: 0.0 ;color: "#00ffffff"}
      GradientStop {position: 0.75;color: "#00ffffff"}
      GradientStop {position: 1.0 ;color: (txt.lines>=0 ? "#ffffffff" : "#00ffffff")}
    }
  }

}
 
