import QtQuick 1.1

Slide {
  titletext: '<b>QtQuick</b>'

  Bullets {
    Bullet {delay: 0;text: '"Declarative" way of building custom, highly dynamic user interfaces with fluid transitions and effects'}
    Bullet {delay: 2;text: 'UI described by <b>QML</b>, logic expressed in embedded <b>Javascript</b>'}
    Bullet {delay: 4;text: '<b>Property binding</b> minimises amount of "plumbing"'}
    Bullet {delay: 6;text: '<b>Native (C++) components</b> easily integrated.'}
  }
}
