import QtQuick 1.1

Slide {
  titletext: 'Mandelbrot C++ sources'
  
  CPPFileText {
    id: header
    source: "Mbrot.h"
    columns: 2
  }

  Rectangle {
    color: '#80888888'
    anchors.left: header.right
    anchors.verticalCenter: parent.verticalCenter
    height: header.height
    width: 1
  }

  CPPFileText {
    source: "Mbrot.cpp"
    anchors.left: header.right
    columns: 2
  }
}
