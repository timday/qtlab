import QtQuick 1.1

Column {
  id: bullets
  x: parent.width/16.0
  width: parent.width-2.0*x
  anchors.verticalCenter: parent.verticalCenter
  spacing: 12*parent.height/600.0
}
