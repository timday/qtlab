import QtQuick 1.1
import Mbrot 1.0

Slide {
  titletext: "C++ components in QML"

  MouseArea {
    anchors.fill: mbrot
    acceptedButtons: Qt.LeftButton|Qt.RightButton
    onDoubleClicked: {
      if (mouse.button==Qt.LeftButton) {
        mbrot.centre=mbrot.positionOfPixel(mouse.x,mouse.y);
        mbrot.radius*=0.5;
      } else {
        mbrot.radius*=2.0;
      }
      mouse.accepted=true;
    }
    onPositionChanged: {
      if (dragx>=0 && dragy>=0) {
        mbrot.downsize=true;
        mbrot.moveCentre(mbrot.positionOfPixel(mouse.x,mouse.y),mbrot.positionOfPixel(dragx,dragy));
      }
      dragx=mouse.x;dragy=mouse.y
    }
    onReleased: {dragx=-1;dragy=-1;mbrot.downsize=false;}
    property real dragx: -1
    property real dragy: -1
  }

  Mbrot {
    id: mbrot
    anchors.fill: parent
    anchors.topMargin: 1.1*parent.headerHeight
    anchors.bottomMargin: 1.1*parent.footerHeight
    colors: [ Qt.rgba(0,0,0,0),Qt.rgba(0,0,0.5,1),Qt.rgba(0,0,1,0),Qt.rgba(0,0,1,1),Qt.rgba(0,0,1,0),Qt.rgba(0,0,1,1)]
  }

  MbrotControlPanel {}
}
