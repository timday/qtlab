import QtQuick 1.1

Slide {
  id: slide
  titletext: "You have been watching..."

  property int n: 17;

  Timer {
    interval: 500
    running: true
    repeat: true
    onTriggered: {
      if (n>=0) {launch(n);n--;}
    }
  }

  function launch(n) {
    
    var digits=(n>=11 ? n+1 : n).toString();   // Skip slide 11; Bar Behavior problem
    while (digits.length<3) digits="0"+digits;
    
    var slideComponent=Qt.createComponent("slide"+digits+".qml");
    var s=slideComponent.createObject(
      slide,
      {
        "state": "GALLERY",
        "scale": 0.2,
	"galleryIndex": n,
        "transformOrigin": Item.Center
      }
    );
  }
}
