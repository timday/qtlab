import QtQuick 1.1

Rectangle {
  id: icon
  radius: 0.25*parent.font.pixelSize
  width: 2.0*radius
  height: 2.0*radius
  x: -1.5*width
  y: 1.5*radius
  border.color: '#4444ff'
  border.width: radius*0.25
  color: '#00000000'
  opacity: parent.opacity
  smooth: true

  SequentialAnimation {
    running: true
    PauseAnimation {
      duration: 1000*bullet.delay
    }
    ParallelAnimation {
      PropertyAnimation {
        duration: 1000
        target: icon
        property: "y"
        from: viewer.height
        to: 1.5*radius
        easing.type: Easing.OutCubic
        //easing.amplitude: 0.5
      }
      PropertyAnimation {
        duration: 1000
        target: icon
        property: "x"
        from: -viewer.height/8
        to: -1.5*width
      }
    }
  }
}
