import QtQuick 1.1

Slide {
  titletext: "More good Qt stuff"
  Bullets {
    Bullet {delay:0;text: "122 QML element types (+7 modules)"}
    Bullet {delay:1;text: '"QtQuick Controls"'}
    SubBullet {delay:2;text: "QML elements for desktop: dialogs, menus, layouts"}
    Bullet {delay:3;text: "QtWebKit/QtWebEngine"}
    Bullet {delay:4;text: "QmlWeb: webapps via DOM & CSS"}
    Bullet {delay:5;text: "QtConcurrent"}
    SubBullet {delay:6;text: "C++ generic map/reduce, QFuture"}
    Bullet {delay:7;text: "PyQt/PySide: Qt python bindings"}
    Bullet {delay:8;text: "Superb documentation"}
  }
}
