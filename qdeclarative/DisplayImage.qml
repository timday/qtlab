import QtQuick 1.1

Image {
  property real finalOpacity: 1.0

  anchors.fill: parent
  anchors.topMargin: 1.1*parent.headerHeight
  anchors.bottomMargin: 1.1*parent.footerHeight
  source: undefined
  fillMode: Image.PreserveAspectFit
  smooth: true

  opacity: 0.0025
  Behavior on opacity {PropertyAnimation{duration: 1000}}
  Component.onCompleted: {opacity=finalOpacity;}
}
