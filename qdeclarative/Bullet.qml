import QtQuick 1.1

SansText {
  id: bullet
  property real delay: 0.0
  height: paintedHeight
  width: parent.width
  text: ""
  font.pixelSize: 36*viewer.height/600.0
  wrapMode: Text.Wrap
  opacity: 0.0025

  SequentialAnimation {
    running: true
    PauseAnimation {
      duration: 1000*delay
    }
    PropertyAnimation {
      target: bullet
      property: "opacity"
      to: 1.0
    }
  }

  BulletIcon {}
}
