#include "QmlViewer.h"

QmlViewer::QmlViewer(const QStringList& slides) 
  :QDeclarativeView(0)
  ,_slide(0)
  ,_slides(slides)
  ,_seconds(0)
{
  setResizeMode(QDeclarativeView::SizeViewToRootObject);
  setRenderHints(QPainter::HighQualityAntialiasing);

  QGLFormat gl_format(QGLFormat::defaultFormat());
  gl_format.setSampleBuffers(true);
  setViewport(new QGLWidget(gl_format));

  rootContext()->setContextProperty("viewer",this);

  load(_slides[_slide]);

  QObject::connect(this,SIGNAL(pressedKeyP()),this,SLOT(print()));
  QObject::connect(this,SIGNAL(pressedKeyQ()),QCoreApplication::instance(),SLOT(quit()));
  QObject::connect(this,SIGNAL(pressedKeyF()),this,SLOT(showFullScreen()));
  QObject::connect(this,SIGNAL(pressedKeyEsc()),this,SLOT(showNormal()));

  QObject::connect(this,SIGNAL(pressedKeySpace()),this,SLOT(toggleTimer()));
  QObject::connect(&_clock,SIGNAL(timeout()),this,SLOT(tick()));

  QObject::connect(this,SIGNAL(contentChanged(const QString&)),this,SLOT(setWindowTitle(const QString&)));

  toggleTimer();
}

QmlViewer::~QmlViewer() {}

void QmlViewer::keyPressEvent(QKeyEvent* event) {
  if (event->key()==Qt::Key_F) {
    emit pressedKeyF();
    event->accept();    
  } else if (event->key()==Qt::Key_P) {
    emit pressedKeyP();
    event->accept();
  } else if (event->key()==Qt::Key_Q) {
    emit pressedKeyQ();
    event->accept();
  } else if (event->key()==Qt::Key_Escape) {
    emit pressedKeyEsc();
    event->accept();
  } else if (event->key()==Qt::Key_Space) {
    emit pressedKeySpace();
    event->accept();
  } 
  QDeclarativeView::keyPressEvent(event);
}

void QmlViewer::resizeEvent(QResizeEvent* event) {
  emit widthChanged(width());
  emit heightChanged(height());
  QDeclarativeView::resizeEvent(event);
}

void QmlViewer::toggleTimer() {
  if (_clock.isActive())
    _clock.stop();
  else {
    emit tick();
    _clock.start(100);
  }
}

QString QmlViewer::getFileContents(const QString& filename) {
  std::cerr << "[Reading file: " << filename.toLocal8Bit().data() << "]";

  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    return QString("File ")+filename+QString(" could not be opened");
  
  return file.readAll();
}

void QmlViewer::move(int n) {
  _slide=(_slide+_slides.length()+n)%_slides.length();
  load(_slides[_slide]);
  emit slideChanged(_slide);
}

void QmlViewer::load(const QUrl& url) {
  QMetaObject::invokeMethod(this,"loadImmediate",Qt::QueuedConnection,Q_ARG(QUrl,url));
}

void QmlViewer::print() {
  QPrinter printer(QPrinter::HighResolution);
  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPageSize(QPrinter::A0);
  printer.setOutputFileName(_slides[_slide].replace(".qml",".pdf"));
  printer.setOrientation(QPrinter::Landscape);
  QPainter painter(&printer);
  render(&painter);
}

void QmlViewer::tick() {
  _seconds+=_clock.interval()/1000.0;
  emit secondsChanged(_seconds);
}

void QmlViewer::loadImmediate(const QUrl& url) {
  std::cerr << "[Setting source " << url.toString().toLocal8Bit().data() << "]";
  setSource(url);

  const QList<QDeclarativeError> errs=errors();
  
  for (int i=0;i<errs.size();i++) {
    std::cerr << "[" << errs[i].toString().toLocal8Bit().data() << "]";
  }

  emit contentChanged(url.toString());
}
