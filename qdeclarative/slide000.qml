import QtQuick 1.1

Slide {
  titletext: ""

  DisplayImage {
    source: "icon/qtlogo.svg"
    finalOpacity: 0.25
  }

  TitleText {
    text: "A Quick Look at QtQuick"
    subtext: "Tim Day"
  }
}
