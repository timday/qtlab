import QtQuick 1.1

Slide {
  titletext: "Property binding demo"

  function hailstone(n) {return (n%2==0 ?  n/2 : 3*n+1);} // Generates next number in "Hailstone" sequence

  Keys.onPressed: {
    if (event.key==Qt.Key_Up && bar0.value<100) {bar0.value++;event.accepted=true;}
    else if (event.key==Qt.Key_Down && bar0.value>1) {bar0.value--;event.accepted=true;}
  }
  Row {
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    width: 0.95*parent.width
    height: 0.95*parent.bodyHeight
    spacing: 2
    Bar {id: bar0 ;value: 42;}                     // First bar value is under keyboard control
    Bar {id: bar1 ;value: hailstone( bar0.value);} // Subsequent bars' values calculated from preceding neighbour
    Bar {id: bar2 ;value: hailstone( bar1.value);}
    Bar {id: bar3 ;value: hailstone( bar2.value);}
    Bar {id: bar4 ;value: hailstone( bar3.value);}
    Bar {id: bar5 ;value: hailstone( bar4.value);}
    Bar {id: bar6 ;value: hailstone( bar5.value);}
    Bar {id: bar7 ;value: hailstone( bar6.value);}
    Bar {id: bar8 ;value: hailstone( bar7.value);}
    Bar {id: bar9 ;value: hailstone( bar8.value);}
    Bar {id: bar10;value: hailstone( bar9.value);}
    Bar {id: bar11;value: hailstone(bar10.value);}
    Bar {id: bar12;value: hailstone(bar11.value);}
    Bar {id: bar13;value: hailstone(bar12.value);}
    Bar {id: bar14;value: hailstone(bar13.value);}
    Bar {id: bar15;value: hailstone(bar14.value);}
  }
}
