import QtQuick 1.1

Item {
  id: bubbles
  anchors.fill: parent
  
  Timer {
    interval: 50
    running: true
    repeat: true
    onTriggered: {if (Math.random()<0.1) launch();}
  }
  
  function launch() {
    var r=bubbles.height*Math.random()*0.1;
    var x=-radius+bubbles.width*Math.random();
    var component=Qt.createComponent("Bubble.qml");
    var bubble=component.createObject(bubbles,{"radius":r,"x":x});
  }
}
