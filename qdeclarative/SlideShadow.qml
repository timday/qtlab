import QtQuick 1.1

Rectangle {
  radius: 0.025*slide.width
  id: shadow
  x: 2.0*radius
  y: 2.0*radius
  width: parent.width
  height: parent.height
  color: '#000000'
  opacity: 0.125
  visible: true
  smooth:true
}
